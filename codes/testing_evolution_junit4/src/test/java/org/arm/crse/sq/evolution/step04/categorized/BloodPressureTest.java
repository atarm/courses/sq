package org.arm.crse.sq.evolution.step04.categorized;

import org.arm.crse.sq.evolution.BloodPressure;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BloodPressureTest {
    //定义被测类
    BloodPressure testObj;

    @Before
    public void setUp() throws Exception {
        testObj = new BloodPressure();
    }

    @After
    public void tearDown() throws Exception {
        testObj = null;
    }

    @Category({ECPTesting.class})
    @Test
    public void getPressureLevel_Normal() {
        testObj.setValues(100, 70);
        String expected = "正常";
        assertEquals(expected, testObj.getLevel());
    }

    @Category({ECPTesting.class})
    @Test
    public void getPressureLevel_NormalHigher() {
        testObj.setValues(130, 85);
        String expected = "正常高值"; 
        assertEquals(expected, testObj.getLevel());
    }

    @Category({ECPTesting.class})
    @Test
    public void getPressureLevel_FirstLevel() {
        testObj.setValues(150, 95);
        String expected = "1级高血压";
        assertEquals(expected, testObj.getLevel());
    }

    @Category({ECPTesting.class})
    @Test
    public void getPressureLevel_SecondLevel() {
        testObj.setValues(170, 105);
        String expected = "2级高血压";
        assertEquals(expected, testObj.getLevel());
    }

    @Category({ECPTesting.class})
    @Test
    public void getPressureLevel_ThirdLevel() {
        testObj.setValues(190, 120);
        String expected = "3级高血压";
        assertEquals(expected, testObj.getLevel());
    }

    @Category({BVATesting.class})
    @Test
    public void getPressureLevel_Boundary1() {
        testObj.setValues(119, 70);
        String expected = "正常";
        assertEquals(expected, testObj.getLevel());
    }

    @Category({BVATesting.class})
    @Test
    public void getPressureLevel_Boundary2() {
        testObj.setValues(120, 85);
        String expected = "正常高值";
        assertEquals(expected, testObj.getLevel());
    }

    @Category({BVATesting.class})
    @Test
    public void getPressureLevel_Boundary3() {
        testObj.setValues(121, 85);
        String expected = "正常高值";
        assertEquals(expected, testObj.getLevel());
    }

    @Category({BVATesting.class})
    @Test
    public void getPressureLevel_Boundary4() {
        testObj.setValues(139, 85);
        String expected = "正常高值";
        assertEquals(expected, testObj.getLevel());
    }

    @Category({BVATesting.class})
    @Test
    public void getPressureLevel_Boundary5() {
        testObj.setValues(140, 85);
        String expected = "1级高血压";
        assertEquals(expected, testObj.getLevel());
    }

    @Category({BVATesting.class})
    @Test
    public void getPressureLevel_Boundary6() {
        testObj.setValues(141, 85);
        String expected = "1级高血压";
        assertEquals(expected, testObj.getLevel());
    }

    @Category({BVATesting.class})
    @Test
    public void getPressureLevel_Boundary7() {
        testObj.setValues(159, 85);
        String expected = "1级高血压";
        assertEquals(expected, testObj.getLevel());
    }

    @Category({BVATesting.class})
    @Test
    public void getPressureLevel_Boundary8() {
        testObj.setValues(160, 85);
        String expected = "2级高血压";
        assertEquals(expected, testObj.getLevel());
    }

    @Category({BVATesting.class})
    @Test
    public void getPressureLevel_Boundary9() {
        testObj.setValues(179, 85);
        String expected = "2级高血压";
        assertEquals(expected, testObj.getLevel());
    }

    @Category({BVATesting.class})
    @Test
    public void getPressureLevel_Boundary10() {
        testObj.setValues(180, 85);
        String expected = "3级高血压";
        assertEquals(expected, testObj.getLevel());
    }

    @Category({BVATesting.class})
    @Test
    public void getPressureLevel_Boundary11() {
        testObj.setValues(181, 85);
        String expected = "3级高血压";
        assertEquals(expected, testObj.getLevel());
    }
}