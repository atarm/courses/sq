package org.arm.crse.sq.evolution.step07.parameterized.categorized;


import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Categories.class)
@Categories.IncludeCategory({SampleCategory.class})
@Suite.SuiteClasses({BloodPressureTest.class})
public class TestSuite {
}
