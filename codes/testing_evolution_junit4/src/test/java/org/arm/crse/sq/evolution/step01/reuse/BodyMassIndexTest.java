package org.arm.crse.sq.evolution.step01.reuse;

import org.arm.crse.sq.evolution.BodyMassIndex;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * this test script will run failure
 */
public class BodyMassIndexTest {

    private BodyMassIndex testObj;

    @Test
    public void testGetBMIType_Thin() {
        testObj = new BodyMassIndex(45.0, 1.6);
        String expected = "偏瘦";
        assertEquals(expected, testObj.getLevel());
    }

    @Test
    public void testGetBMIType_Normal() {
        testObj.setValues(55.0, 1.6);
        String expected = "正常";
        assertEquals(expected, testObj.getLevel());
    }

    @Test
    public void testGetBMIType_SlightlyFat() {
        testObj.setValues(68.0, 1.6);
        String expected = "偏胖";
        assertEquals(expected, testObj.getLevel());
    }

    @Test
    public void testGetBMIType_Fat() {
        testObj.setValues(80.0, 1.6);
        String expected = "肥胖";
        assertEquals(expected, testObj.getLevel());
    }
}