package org.arm.crse.sq.evolution.step03.parameterized.pi;

import org.arm.crse.sq.evolution.BodyMassIndex;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * Property Inject
 */
@RunWith(Parameterized.class)
public class BodyMassIndexTest {
    @Parameterized.Parameter(0)
    public double weight;

    @Parameterized.Parameter(1)
    public double height;

    @Parameterized.Parameter(2)
    public String expected;
    BodyMassIndex testObj;

    public BodyMassIndexTest() {
    }

    /**
     * test data
     *
     * @return
     */
    @Parameterized.Parameters
    public static Collection testDataset() {
        return Arrays.asList(
            new Object[][]{
                {45.0, 1.6, "偏瘦"},
                {55.0, 1.6, "正常"},
                {68.0, 1.6, "偏胖"},
                {80.0, 1.6, "肥胖"}
            }
        );
    }

    @Before
    public void setUp() throws Exception {
        testObj = new BodyMassIndex(weight, height);
    }

    @After
    public void tearDown() throws Exception {
        testObj = null;
    }

    @Test
    public void getBMIType() {
        assertEquals(expected, testObj.getLevel());
    }
}
