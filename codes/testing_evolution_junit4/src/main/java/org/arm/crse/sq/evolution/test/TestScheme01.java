package org.arm.crse.sq.evolution.test;

import org.arm.crse.sq.evolution.BodyMassIndex;

import java.util.Scanner;

/**
 * 测试方案01：
 * 1. 使用 “main”函数
 * 2. 用户输入或自动输入身高、体重
 * 3. 调用被测方法
 * 4. 将返回值输出到屏幕
 */
public class TestScheme01 {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        double w = 0.0, h = 0.0;
        System.out.println("请输入体重和身高，以等号=结束");
        while (reader.hasNextDouble()) {
            w = reader.nextDouble();
            h = reader.nextDouble();
        }
        BodyMassIndex testObj = new BodyMassIndex(w, h);
        String result = testObj.getLevel();
        String output = "体重：" + w + "，身高：" + h + "，BMI状况是：" + result;
        System.out.println(output);

        //自动输入测试数据
        BodyMassIndex tmpObj = new BodyMassIndex();

        tmpObj.setValues(45.0, 1.6);
        System.out.println(tmpObj.getLevel());

        tmpObj.setValues(55.0, 1.6);
        System.out.println(tmpObj.getLevel());

        tmpObj.setValues(68.0, 1.6);
        System.out.println(tmpObj.getLevel());

        tmpObj.setValues(80.0, 1.6);
        System.out.println(tmpObj.getLevel());
    }
}
