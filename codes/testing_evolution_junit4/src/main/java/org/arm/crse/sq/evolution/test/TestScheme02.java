package org.arm.crse.sq.evolution.test;

import org.arm.crse.sq.evolution.BodyMassIndex;

/**
 * 测试方案02：
 * 1. 使用“main”函数
 * 2. 自行设置输入值（身高、体重）
 * 3. 调用被测方法
 * 4. 校验执行结果： 自行比较实际值和预期值
 * 5. 主动报告缺陷
 */
public class TestScheme02 {

    public static void main(String[] args) {
        // 方案2：
        // 脚本自行根据测试用例设计来设置体重和身高，并自动校验执行结果
        // 1.创建被测对象
        BodyMassIndex testObj = new BodyMassIndex(45.0, 1.6);

        // 2.调用被测方法（使用测试用例）
        String actual = testObj.getLevel();

        // 3.校验执行结果
        String expected = "偏瘦";
        String output = "";

        //TODO: how to improve it?
        Boolean isEquals = (expected == actual);

        // 4. 判断测试结果
        if (isEquals) {
            // 5. 记录测试结果
            output += "Pass. 体重:45.0, 身高:1.6";
        } else {
            output += "Fail. 体重:45.0, 身高:1.6, Expected:" + expected + ", Actual:" + actual;
        }
        output += "\n";

        // 测试用例2，测试用例间应该相互独立
        testObj.setValues(55.0, 1.6);
        actual = testObj.getLevel();
        expected = "正常";
        isEquals = actual.equals(expected);
        if (isEquals) {
            output += "Pass. 体重:55.0, 身高:1.6";
        } else {
            output += "Fail. 体重:55.0, 身高:1.6, Expected:" + expected + ", Actual:" + actual;
        }
        output += "\n";

        // 测试用例3
        testObj.setValues(68.0, 1.6);
        actual = testObj.getLevel();
        expected = "偏胖";
        isEquals = actual.equals(expected);
        if (isEquals) {
            output += "Pass. 体重:68.0, 身高:1.6";
        } else {
            output += "Fail. 体重:68.0, 身高:1.6, Expected:" + expected + ", Actual:" + actual;
        }
        output += "\n";

        // 测试用例4
        testObj.setValues(80.0, 1.6);
        actual = testObj.getLevel();
        expected = "肥胖";
        isEquals = actual.equals(expected);
        if (isEquals) {
            output += "Pass. 体重:80.0, 身高:1.6";
        } else {
            output += "Fail. 体重:80.0, 身高:1.6, Expected:" + expected + ", Actual:" + actual;
        }
        output += "\n";

        //4. 输出结果
        System.out.println(output);
    }
}
