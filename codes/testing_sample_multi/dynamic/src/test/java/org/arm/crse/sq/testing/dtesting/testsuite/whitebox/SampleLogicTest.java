package org.arm.crse.sq.testing.dtesting.testsuite.whitebox;

import org.arm.crse.sq.testing.dtesting.example.SampleLogic;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

class SampleLogicTest {
    private SampleLogic testObj;

    @BeforeAll
    static void init() {
    }

    @AfterAll
    static void cleanup() {
    }

    @BeforeEach
    void setUp() {
        testObj = new SampleLogic();
    }

    @AfterEach
    void tearDown() {
        testObj = null;
    }

    @ParameterizedTest
    @CsvFileSource(resources = {
        "/usecases/whitebox/samplelogic_statement_coverage.csv",
        "/usecases/whitebox/samplelogic_branch_coverage.csv",
        "/usecases/whitebox/samplelogic_condition_coverage.csv",
        "/usecases/whitebox/samplelogic_branch-condition_coverage.csv",
    })
    void process(int x, int y, int expect) {
        int actual = testObj.process(x, y);
        Assertions.assertEquals(expect, actual);
    }
}