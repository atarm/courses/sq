package org.arm.crse.sq.testing.dtesting.example;

/** Hello world! */
public class Triangle {
  public static final String EQUILATERAL = "等边三角形";
  public static final String ISOSCELES = "等腰三角形";
  public static final String GENERAL = "一般三角形";
  public static final String NO_A_TRIANGLE = "不是三角形";
  public static final String INPUT_ERROR = "输入有错误";
  public static final int MIN_LENGTH = 1;
  public static final int MAX_LENGTH = 100;

  public String getType(int x, int y, int z) {
    if (!isLengthValid(x) || !isLengthValid(y) || !isLengthValid(z)) {
      return INPUT_ERROR;
    }
    if (x + y > z && x + z > y && y + z > x) {
      if (x == y && y == z) {
        return EQUILATERAL;
      } else if (x == y || x == z || y == z) {
        return ISOSCELES;
      } else {
        return GENERAL;
      }
    } else {
      return NO_A_TRIANGLE;
    }
  }

  private boolean isLengthValid(int len) {
    return !(len < MIN_LENGTH || len > MAX_LENGTH);
  }
}
