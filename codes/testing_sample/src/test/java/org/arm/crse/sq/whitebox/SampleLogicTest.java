package org.arm.crse.sq.whitebox;

import org.arm.crse.sq.SampleLogic;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

class SampleLogicTest {
    private SampleLogic testObj;

    @BeforeAll
    static void init() {

    }

    @AfterAll
    static void cleanup() {

    }

    @BeforeEach
    void setUp() {
        testObj = new SampleLogic();
    }

    @AfterEach
    void tearDown() {
        testObj = null;
    }

    @Disabled
    @ParameterizedTest
    @CsvFileSource(resources = "/usecases/whitebox/samplelogic_statement_coverage.csv")
    void statementCoverageTest(int x, int y, int expect) {
        this.testIt(x, y, expect);
    }

    @Disabled
    @ParameterizedTest
    @CsvFileSource(resources = "/usecases/whitebox/samplelogic_branch_coverage.csv")
    void branchCoverageTest(int x, int y, int expect) {
        this.testIt(x, y, expect);
    }

    //    @Disabled
    @ParameterizedTest
    @CsvFileSource(resources = "/usecases/whitebox/samplelogic_condition_coverage.csv")
    void conditionCoverageTest(int x, int y, int expect) {
        this.testIt(x, y, expect);
    }

    @Disabled
    @ParameterizedTest
    @CsvFileSource(resources = "/usecases/whitebox/samplelogic_logic_coverage.csv")
    void logicCoverageTest(int x, int y, int expect) {
        int actual = testObj.process(x, y);
        Assertions.assertEquals(expect, actual);
    }

    private void testIt(int x, int y, int expect) {
        int actual = testObj.process(x, y);
        Assertions.assertEquals(expect, actual);
    }
}