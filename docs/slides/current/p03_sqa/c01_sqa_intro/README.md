---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Introduction to Software Quality Assurance_"
footer: "@aRoming"
math: katex
---

<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Introduction to Software Quality Assurance

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [软件质量管理与软件质量保证](#软件质量管理与软件质量保证)
2. [软件质量保证体系](#软件质量保证体系)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 软件质量管理与软件质量保证

---

1. 软件质量管理（Software Quality Management，`SQM`）的目标：管理软件开发过程中与开发完成后的软件质量，一个优质的产品必须符合质量的要求条件，并且满足用户的需求。
1. 许多人交替使用`SQM`（软件质量管理）与`SQA`（软件质量保证）概念

>[软件质量管理.](https://zh.wikipedia.org/zh-cn/軟體品質管理)

❗ 应尽可能地区分`SQM`、`SQA`和`SQC` ❗

---

- Software quality management activities are generally split up into three core components: quality assurance, quality planning, and quality control.  

>[Software Quality Management.](https://en.wikipedia.org/wiki/Software_quality_management)

- Software quality control is a function that checks whether a software component, or supporting artifact meets requirements, or is "fit for use". Software Quality Control is commonly referred to as Testing.  

>[Software Quality Control.](https://en.wikipedia.org/wiki/Software_quality_control)

<!--

---

### 软件质量管理的基本概念

1. 软件质量管理是一组由开发组织使用的程序和方法，使用它可在规定的资金投入和时间限制的条件下，提供满足客户质量要求的软件产品并持续不断地改善开发过程和开发组织本身，以提高将来生产高质量软件产品的能力
    1. 软件质量管理是开发组织执行的一系列过程
    1. 软件质量管理的目标是以最低的代价获得客户满意的软件产品
    1. 对于开发组织本身来说，软件质量管理的另一个目标是从每一次开发过程中学习，以便使软件质量管理效果一次比一次好

-->

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 软件质量保证体系

---

### `SQA`的定义

1. `SQA`是建立一套有计划，有系统的方法，来向干系人（管理层或客户或用户）保证拟定出的标准、步骤、实践和方法能够正确地被所有项目所采用。软件质量保证的目的是使软件过程对于干系人来说是可见的
    1. 通过对软件产品和活动进行评审和审计来验证软件是合乎标准的、满足机构方针的要求
    1. 在项目开始时就一起参与建立计划、标准和过程
    1. 目的是向干系人提供对软件过程进行全面监控的手段，包括评审和审计软件产品和活动，验证它们是否符合相应的规程和标准，同时给干系人提供这些评审和审计的结果

---

### `SQA`与`CMM2`

1. `SQA`是`CMM2`中的一个重要关键过程区域，它是贯穿于整个软件过程的第三方独立审查活动，在CMM的过程中充当重要角色
1. 满足`SQA`是达到`CMM2`要求的重要步骤之一

---

![height:550](./assets_image/content_of_SQA.png)

---

### `SQA`的背景

1. 1916：`Bell Labs`出现第一个正式的质量保证和控制职能部门，开始风靡制造业
1. 1970s：软件质量标准首次出现在军方的软件开发合同中，开始广泛应用

---

### `SQA`的目标

1. `SQA`的目标：以独立审查的方式监控软件生产任务的执行，给开发人员和管理层提供反映产品质量的信息和数据，辅助软件组织生产出高质量的软件产品

---

### `SQA`的职责

1. `SQA`不负责生产高质量的软件产品和制定产品质量计划（协助制订但不负责制定）
1. `SQA`负责评审和审计软件开发组织的质量活动、并鉴别活动中出现的偏差
    1. 通过监控软件过程保证产品质量
    1. 保证软件过程符合相应的标准和规程
    1. 保证软件过程存在的问题得到恰当的处理，必要时将问题反映给管理层

---

### `SQA`的保证内容

1. 行为
    1. 🌟 独立的审计/审查
1. 内容
    1. 选定的开发方法被采用、选定的标准和规则得到采用和遵循
    1. 定义的任务得到实际执行
    1. 偏离标准和规程的问题得到及时地反映和处理

---

### `SQA`负责的活动/工作内容

1. `SQA`计划
1. `SQA`评审
1. `SQA`监控
1. `SQA`报告
1. `SQA`跟踪
1. `SQA`审计

---

### `SQA`的主要方法和技术

1. 静态测试技术
    1. 分析技术`analysis`
        1. 数据收集：监控、检查
        1. 数据分析
        1. 数据可视化
    1. 检查技术`inspections`
    1. 评审技术`reviews`
1. 审计技术`audits`

---

### `SQA`参与的活动/工作内容

1. 协助识别质量需求
1. 协助制订项目计划
    1. 回复有关项目标准和规程的咨询
    1. 验证项目计划、标准和规程是否到位
    1. 参与项目计划评审

---

### SQA的实施因素/先决条件

1. `SQA`人员的素质和经验：执行过程抓住问题的重点和本质
1. 组织已建立标准和规则
1. 管理层重视`SQA`

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
