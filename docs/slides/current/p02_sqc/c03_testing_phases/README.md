---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "__"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# 测试阶段Testing Phases

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [主要内容Contents](#主要内容contents)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 主要内容Contents

---

<!-- ```bash {.line-numbers cmd=true output=text hide=true run_on_save=true}
tree -d .
``` -->

```bash {.line-numbers}
Testing Phases
.
├── s01_low-level_testing        低阶测试
├── s02_high-level_testing       高阶测试
└── s03_regression_testing       回归测试
```

---

1. 低阶测试：单元测试、集成测试
1. 高阶测试：系统测试、验收测试

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
