---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Testing Management and Testing Process_"
footer: "@aRoming"
math: katex
---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# 测试管理与测试流程Testing Management and Testing Process

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [测试管理体系与人员组织Test Management System and Organization](#测试管理体系与人员组织test-management-system-and-organization)
2. [🌟 测试过程与测试文档Test Processes and Documents](#测试过程与测试文档test-processes-and-documents)
3. [测试分析与测试改进Test Analysis and Test Improvement](#测试分析与测试改进test-analysis-and-test-improvement)

<!-- /code_chunk_output -->

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 测试管理体系与人员组织Test Management System and Organization

---

<!--### 为什么要进行测试管理-->

<!--TODO: improve it-->

1. 软件规模扩大、复杂度增加，测试工作越来越困难 ==> 从点到面
1. 测试的实施在软件实现后进行，但测试的准备工作在需求、设计阶段就需要开始

---

### 测试管理的对象

1. 资源：人力资源和环境资源
1. 过程：测试过程
1. 结果：测试文档、软件缺陷

---

### 规范的测试及其管理系统的特点

1. `SQC`
    1. 软件产品的验证、监视和度量
    1. 软件产品缺陷的识别和跟踪
1. `SQA`
    1. 软件测试过程的验证、监视和度量
    1. 软件测试过程缺陷的识别和跟踪

---

### 测试管理框架

![height:500](./.assets/image/test_mgt_framework.png)

---

### 测试项目组织结构

---

#### 测试项目组织结构的设计 – 组织机构设置原则

1. 目的性原则
1. 高效精干原则
1. 一体化组织原则

---

#### 项目组织结构 – 按成员来源划分

1. 专门的项目部门：专职的管理职责人员
    1. 项目型项目组织
1. 临时的项目团队
    1. 工作队式项目组织
    1. 部门控制式项目组织
1. 虚拟的项目团队：成员非全职参与项目
    1. 矩阵型项目组织

---

### 项目组织结构 – 按汇报关系划分 – 01

![height:300](./.assets/diagram/organization_01.svg)

---

### 项目组织结构 – 按汇报关系划分 – 02

![height:400](./.assets/diagram/organization_02.svg)

---

### 项目组织结构 – 按汇报关系划分 – 03

![height:300](./.assets/diagram/organization_03.svg)

---

### 测试人员组织

- 软件评审
    1. `SRS`评审：系统分析员、软件开发管理者、开发工程师、测试工程师、用户代表
    1. `SDD(Software Design Description)`评审： 系统分析员、软件架构师/软件设计师、测试负责人
- 软件测试
    1. 系统测试：测试组组长、测试设计工程师、测试工程师

---

### 测试管理者工作原则

1. 雇用测试工作最合适的员工
1. 与每个小组成员定期一对一谈话
1. 假定员工都能胜任各自的测试工作
1. 对待员工以他们能接受的方式
1. 重视工作结果而不是工作时长
1. 承认自己的错误

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 🌟 测试过程与测试文档Test Processes and Documents

---

### File vs Document vs Documentation

1. `file`: `n.`，文件，包括文字文件及其他格式的文件
1. `document`： `n.`，公文、文档，一般指文字文件
1. `documentation`： `un.`，正式的文献、资料等的总称

---

### 测试文档

- 测试文档：用于规范测试或描述要执行的测试及测试的结果
- 文档类型：组织资产和项目资产

---

#### 测试文档 – 组织资产

1. 测试规范
1. 测试样例

---

##### 测试规范

1. 背景信息：规范文档路径
1. 测试特性
1. 功能考虑
1. 测试考虑
1. 测试假定

---

#### 测试文档 – 项目资产

1. 计划类：从需求阶段开始编制，到设计阶段结束时完成
    1. 测试计划书
    1. 测试方案（含测试用例）
1. 报告类：测试阶段内编写
    1. 测试报告
    1. 缺陷报告
    1. 缺陷跟踪表
    1. 测试分析报告

---

### 测试过程/测试生命周期

1. Plan
    1. 测试需求 ==> 测试范围和测试任务
    1. 测试计划 ==> 测试计划书
1. Do
    1. 测试设计 ==> 测试方案/测试用例集
    1. 测试执行 ==> 测试日志、测试报告（含缺陷报告）
    1. 缺陷跟综 ==> 缺陷跟综表
1. Check
    1. 测试分析 ==> 测试分析报告
1. Adjust
    1. 测试总结

---

![height:600](./.assets/diagram/testing_lfiecycle.svg)

---

#### 测试计划

1. 作为产品交付的测试计划
1. 作为内部管理的测试计划
1. 测试计划
    - 目的：为了高效地、高质量地完成测试任务而做的准备工作
    - 内容：测试工作量估算、测试资源和测试进度安排、测试风险评估、测试策略制定等工作
    - 前置：测试需求分析

---

#### 微软测试计划样例

1. 测试计划概要
1. 目标和发布标准
1. 测试范围/测试领域
1. 测试方法
1. 测试进度计划
1. 测试资源/测试人员
1. 测试环境/测试配置
1. 测试工具

---

#### 测试用例

1. 基本信息
    1. ID
    1. 简要描述
1. 前置条件
1. 输入
1. 测试步骤
1. 预期输出

<!--TODO: add it

-->

---

#### 测试报告

1. 引言： 项目背景、参考资料
1. 测试基本信息： 测试范围、测试用例设计思路
1. 测试结果与缺陷分析： 测试执行情况与记录、缺陷的统计与分析
1. 测试结论与建议： 风险分析及建议、测试结论
1. 附属文档

---

#### 缺陷报告

1. 缺陷简述
1. 优先级与严重性
1. 缺陷产生背景：软件部署环境、软件版本
1. 输入及详细测试步骤
1. 正常情况应有的结果（预期结果）
1. 缺陷造成的后果（实际结果）
1. 其他信息

---

#### 软件测试后的软件调试

---

1. 调试不是测试
1. 测试发现缺陷之后需要进行调试以定位和解决缺陷

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 测试分析与测试改进Test Analysis and Test Improvement

---

### DRE(Defect Removal Efficiency, 缺陷排除效率)

1. `DRE`在项目级和过程级都能提供有益的质量度量
1. `DRE`是对质量保证及控制活动的过滤能力的一个测量，这些活动贯穿于整个过程框架活动
1. $DRE=E/(E＋D)$
    - E： 软件交付前所发现的缺陷数量
    - D： 软件交付后所发现的缺陷数量

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
