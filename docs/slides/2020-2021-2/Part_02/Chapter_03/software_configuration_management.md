---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Software Configuration Management_"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 软件配置管理

## Software Configuration Management

---

## Contents

1. :star2: 软件配置管理概述：起源、目标、工具、对象
1. 软件配置管理的角色、过程和关键活动

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 软件配置管理概述

><https://en.wikipedia.org/wiki/Configuration_management>

:point_right: 没有记录下来的事物则无法度量 :point_left:

---

### `SCM`的起源

1. `CM`起源自美国空军，为规范设备的设计与制造，于1962年制定第一个配置管理标准 _"AFSCM375-1, CM During the Development & Acquisition Phases"_
1. `SCM`于1960s末至1970s初提出
    1. `UCSB`的`Leon Presser`撰写的 _"Change and Configuration Control"_ 论文，提出控制变更和配置的概念，同时将控制变更和配置应用到撰写该论文的项目中（该论文进行了近1400万次修改）
    1. `Leon Presser`在1975年成立`SoftTool`公司，开发了最早的配置管理工具之一`CCC（Change and Configuration Control）`
1. 国内于2000s前后随着`BAT`等互联网企业的发展而起步

---

1. `SCM`从最基础的版本控制功能发展到包括工作空间管理、并行开发支持、过程管理、权限控制、变更管理等一系列全面的管理功能，形成了一个完整的理论体系

:warning: `SCM`在一些场景下也可能是`Source Control Management`的缩写，在该场景下`SCM`指一个专门管理`source`的并实现了`VCS`的软件 :warning:

---

### `SCM`与`CMM`

1. 软件配置管理作为`CMM 2`的一个KPA`(Key Practice Area, 关键域)`，在整个软件的开发活动中占有重要的位置
1. 软件配置管理是贯穿于整个软件过程中的保护性活动，它被设计来：
    1. 标识变更
    1. 控制变更
    1. 跟踪变更/确保变更被适当地实现
    1. 报告变更/向干系人报告变更状态

---

### `SCM`工具

1. `VCS(Version Control System)`
    1. centralized/client-server: `CVS(Concurrent Version System)`, `VSS(Visual Source Safe)`(6.0 released at June 3, 1998), `SVN(Subversion)`
    1. distributed: `git`, `mercurial`
1. `SCM(Software Configuration Management)`
    1. `project-level SCM`: `PVCS`, `MKS`
    1. `enterprise-level SCM`: `CCC Harvest`, `Rational ClearCase`

---

### 配置管理案例 ==> 点菜

1. 服务员将你所点的菜记下来（配置项标识）
1. 服务员给你重复一遍确认（建立基线）
1. 服务员发给后厨10分钟后，你要加个菜（配置项变更）
1. 服务员再次点菜重复确认（变更确认）
1. 吃完饭结账拿账单给你确认（状态确认）
    1. 没问题则进入付款流程
    1. 有问题则进入变更流程

---

### `SCM`的对象

+ `SCM` ==> 关于软件资产的管理
    1. `software` = `program` + `data` + `document`
    1. `program` = `algorithm` + `data structure`
+ `configuration` ==> the `configuration` of the `software production`
+ `SCI(Software Configuration Item)` ==> `item` of the `configuration`

><https://www.jianshu.com/p/f8d81680a7cf>

---

### `SCM`的目标

+ `SCM`是在贯穿整个`SDLC`中建立和维护项目产品的完整性
+ `SCM`的基本目标包括：
    1. 被选择的`SCI`得到识别
    1. 各`SCI`的各项工作有计划进行着
    1. 已识别出的`SCI`的更改得到控制
    1. 干系人及时了解`Base Line`的状态和内容

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `SCM`的角色、过程、活动

---

### `SCM`的角色

1. `PM`(Project Manager, 项目经理)
1. `CCB`(Configuration Control Board，配置控制委员会)/`CCB`(Change Control Board, 变更控制委员会)
1. `CMO`(Configuration Management Officer, 配置管理员)/`CMA`(Configuration Management Administrator)
1. `SIO`(System Integration Officer, 系统集成员)/`SIA`(System Integration Administrator)
1. `DEV`(Developer, 开发人员)

---

### `SCM`的过程描述

1. 计划阶段
    + `CCB`根据项目的开发计划确定各个里程碑和开发策略
    + `CMO`根据`CCB`的规划，制定详细的`SCM`计划，交`CCB`审核
    + `CCB`通过配置管理计划后交`PM`批准，发布实施
1. 执行阶段
    + 主要由`CMO`完成管理和维护工作
    + 由`SIO`和`DEV`具体执行`SCM`策略
    + 变更流程

---

![height:450](./assets_image/software_configuration_management_01.png)

---

![height:600](./assets_image/software_configuration_management_02.png)

---

![height:600](./assets_image/scm_processes.png)

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `SCM`的关键活动

---

#### `SCM`的活动涉及的关键问题

1. **_如何管理版本_**： 一个组织如何标识和管理程序（及其文档）的很多现存版本，以使得变化可以高效地进行？
1. **_如何管理版本的变化_**： 一个组织如何在软件被发布给客户之前之后控制变化？
    + **_如何控制变化_**： 谁负责批准变化，并给变化确定优先级？
    + **_如何跟踪变化_**： 我们如何保证变化已经被恰当地进行？
    + **_如何发布变化_**： 采用什么机制去告知其他人员已经实行的变化？

---

#### `SCM`的活动

1. 配置项识别
1. 工作空间管理
1. 版本控制
1. 变更控制
1. 状态报告
1. 配置审计

---

#### 配置项识别Identify Software Configuration Item

1. 配置项（软件过程的输出信息）类型
    1. 程序： 源代码、可执行程序
    1. 文档： 开发者文档、用户文档
    1. 数据： 内部数据、外部数据
1. 基线`Base Line`： 已正式通过评审和批准的某规约或产品，因此可作为进一步开发的基础，并且只能通过正式的变更控制流程才能变更

---

1. 将`SDLC`中需要加以控制的配置项分为基线配置项和非基线配置项
1. 所有的配置项都应按照一定的规定统一编号、按照相应的模板生成，并在文档中的规定章节记录对象的标识信息

---

#### 工作空间管理

1. 所有人员都被要求将工作成果存放到`SCM`配置库中，或直接工作在`SCM`提供的环境下
1. 所有人员按任务的要求在不同的阶段工作在不同的工作空间中

---

#### 版本控制

1. :star2: 版本控制是`SCM`的核心功能
1. 配置库中的元素应自动给予版本标识，并保证版本命名的唯一性
1. 版本生成过程中依照设定的模型分支和演进
1. `SCM`进阶功能： 将相关数据统计成过程数据，从而方便`SPI`(Software Process Improvement, 软件过程改进)

---

#### 变更控制

1. 基线和变更控制紧密相连
1. 变更控制控制的对象是`SCI`，特别是`SCI`中的`base line`

---

#### 状态报告

1. 根据`SCM`的操作记录向管理者报告软件开发活动的进展情况
1. 状态报告应着重反映当前`base line SCI`的状态，作为对开发进度报告的参照

---

#### 状态报告的内容

1. 配置库结构和相关说明
1. 开发起始基线的构成
1. 当前基线位置及状态
1. 各分支的情况
1. 关键元素版本的演进记录
1. 其他应报告的事项

---

#### 配置审计

1. 配置审计的主要作用是作为变更控制的补充手段确保某一变更需求已被切实在执行
1. 配置审计可作为正式的技术评审的一部分

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok:End of This Slide:ok:
