---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Software Review_"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 软件评审Software Review

><https://en.wikipedia.org/wiki/Software_review>

---

## Contents

1. 软件评审概述
1. 软件评审的角色和职能
1. 软件评审的内容
1. :star2: 软件评审的方法和技术
1. 软件评审的会议流程
1. :star2: 代码评审/代码审查

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 评审概述

---

+ `SQC`角度
    1. 评审属于静态测试
    1. 评审属于人工测试
+ `SQA`角度
    1. 评审是其中一个重要活动

---

### 为什么需要软件评审

1. 软件缺陷一半以上是由于不正确的设计规范引起的
1. 软件评审的作用/意义：
    1. 提高项目生产率：
        1. 早期发现错误，减少返工时间
        1. 加深开发团队对产品和开发过程的熟悉度
        1. 提升开发团队的个人能力
    1. 改善软件质量，提升产品可维护性
    1. :star2: 通过正式的评审标志着一个阶段的完成

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 软件评审的角色和职能

---

1. 宣读员reader/主持人host
1. 记录员recorder
1. 作者author
1. 评审组长moderator
1. 评审员reviewer/inspector

---

### 评审员的主要职责和注意事项

+ 主要职责：
    1. 会前熟悉评审内容，为评审做好准备
    1. 会前或会后就存在的问题提出建设性的意见和建议
+ 注意事项：
    1. 针对问题而不是针对个人
    1. 主要的问题和次要的问题可以被分别讨论
    1. 明确自己的角色和责任
    1. 做好接受错误的准备

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 软件评审的内容

---

1. 管理评审： 组织的管理者就管理体系的现状、适宜性、充分性和有效性以及方针和目标的贯彻落实及实现情况进行正式的评价
1. 技术评审： 评审者按照规范的步骤对软件需求、设计、代码或其他技术文档进行仔细的检查，以找出和消除其中的缺陷
1. 过程评审： 通过对过程的监控保证`SQA`定义的软件过程在项目中得到遵循
1. 文档评审 :question:

:question: 技术评审与文档评审的区别是什么 :question:

---

### 管理评审

+ 目的：
    1. 通过这种评审活动来总结管理体系的业绩，并从当前业绩上考虑找出与预期目标的差距
    1. 考虑任何可能改进的机会，并在研究分析的基础上，对组织在市场中所处地位及竞争对手的业绩予以评价，从而找出自身的改进方向

---

### 技术评审

+ 技术评审是一种同行审查技术，在技术评审后需以书面形式对评审结果进行总结
+ 主要特点：由一组评审员按照规范的步骤对软件需求、设计、代码或其他技术文档进行仔细的检查，以找出和消除其中的缺陷
+ 主要目的：确保需求说明、设计说明书与最初的说明书一致，并按照计划对软件进行了正确的开发

---

### 过程评审

1. 进行过程评审，需要成立一个专门的过程评审小组
1. 评审小组要走访软件生产涉及的各个部门和人员，包括开发工程师、测试工程师，甚至兼职人员等

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 评审的方法和技术

---

### :star2: 评审的方法

+ 从形式的严谨程度，可分为正式评审和非正式评审
+ 非正式评审
    1. `ad hoc review非正式检查/随机检查`
    1. `pass around轮查`
    1. `walk through走查`
+ 正式评审
    1. `group review团队评审`
    1. `inspection审视/审查`

:point_right: `ad hoc`: 非事先计划（的）/随机（的）；非正式（的）:point_left:

---

### `Ad Hoc Review` and `Pass Around`

+ `ad hoc review`： 最不正式的评审方法，通常应用于平时的小组合作
+ `pass around`
    1. 作者向评审者做简要介绍，但不参加评审过程
    1. 评审者独立进行评审，记录发现的结果形成报告

---

|角色/职责|审视|团队评审|走查|
|--|--|--|--|
|主持人|评审组长|评审组长或作者|作者|
|材料陈述者|评审者|评审组长|作者|
|记录员|是|是|可能|
|专门的评审角色|是|是|否|
|检查表|是|是|否|
|产品评估|是|是|否|

---

|角色/职责|审视|团队评审|走查|
|--|--|--|--|
|计划|有|有|是|
|准备|有|有|无|
|会议|有|有|有|
|修正|有|有|有|
|问题跟踪|**有**|**有**|无|
|问题分析|**有**|**_可能_**|无|
|确认|有|有|无|

---

### 评审的工具和方法

1. 工具
    1. 缺陷检查表与规则集
    1. 缺陷跟踪表
    1. 评审辅助工具
1. 方法
    1. 多视角观察法
    1. 场景分析法

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 评审会议流程

---

1. 准备评审会议
1. 召开评审会议
1. 跟踪分析评审结果

---

### 准备评审会议

+ 会议信息： 时间、地点、人物、事件/主题
+ 会议材料：
    1. 选择评审材料
    1. 收集打包评审材料
    1. 分发评审材料

---

### 召开评审会议

1. 评审预备
1. 评审开始
1. 评审决议
1. 评审结束

---

### 跟踪分析评审结果

+ 跟踪评审结果： 基于缺陷列表
+ 分析评审结果： 有效性分析、效率与成本分析

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 代码评审/代码审查Code Review

---

### 代码评审是什么What is Code Review

>1. `code review` (sometimes referred to as `peer review`) is a `software quality assurance` activity in which _**one or several people**_ check a program mainly by _**viewing and reading parts of its source code**_, and they do so after implementation or as an interruption of implementation.
>1. _**at least**_ one of the persons must not be the code's author.
>1. the persons performing the checking, excluding the author, are called "reviewers".
>
>[wiki.code review](https://en.wikipedia.org/wiki/Code_review)

---

>同行评审（peer review，在某些学术领域亦称refereeing），或译为 同行评议、同侪评阅、同侪审查，是一种学术成果审查程序，即一位作者的学术著作或计划被同一领域的其他专家学者评审。同行评审程序的主要目的是：确保作者的著作水平符合一般学术与该学科领域的标准。
>
>一般学术出版单位，主要以同行评审的方法来选择与筛选所投送的稿件录取与否；而学术研究资金提供机构，也广泛以同行评审的方式来决定研究是否授予资金、奖金等。在许多领域，著作的出版或者研究奖金的颁发，如果没有以同行评审的方式来进行就可能比较会遭人质疑，甚至成为某出版物、作品是否可以被称为学术出版物的主要标准。
>
>[wiki.同行评审](https://zh.wikipedia.org/wiki/同行評審)

---

### 代码评审的主要工作

1. 发现代码中的`bug`
1. 检查是否符合风格规范
1. 从代码的易维护性、可扩展性角度考察代码的质量
1. 提出修改建议

---

### 代码评审的责任人

1. 代码作者承担主要责任
1. 代码评审员承担次要责任

---

### 代码评审的流程（传统方式）

1. 代码作者对照`SRS`讲解代码逻辑
1. 代码评审员可随时提出疑问，并记录在案
1. 代码作者讲解完后，代码评审员进行代码审查
1. 代码评审员根据审查结果编写`“代码审查报告”`并发送给相关人员
1. 代码作者根据`“代码审查报告”`修改代码
1. 代码作者反馈`bugfix`
1. 重回初始步骤，直至不再发现`bug`

---

### 代码评审的流程（基于`Git`方式）

![height:600](./assets/image/pull-based-work-flow-on-github.jpg)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Slide :ok:
