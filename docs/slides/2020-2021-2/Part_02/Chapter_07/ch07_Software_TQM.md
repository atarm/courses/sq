---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Software Total Quality Management_"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 软件全面质量管理Software Total Quality Management

---

## Contents

1. :star2: `戴明环PDCA Cycle/Deming Cycle`
1. :star2: `TQM(Total Quality Management, 全面质量管理)`
1. :star2: `Six Sigma六西格玛/六标准差`
1. `QFD(Quality Function Deployment, 质量功能展开)`
1. `DMAIC(Define, Measure, Analyze, Improve and Control)`与`DFSS(Design For Six Sigma)`

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## :star2: 戴明环PDCA Cycle/Deming Cycle

---

1. `P(Plan)`: establish objectives and processes required to deliver the desired results
1. `D(Do)`: carry out the objectives from the previous step
1. `C(Check)`: the data and results gathered from the `do` phase are evaluated, data is compared to the expected outcomes to see any similarities and differences.
1. `A(Act/Adjust)`: where a process is improved

>[Wikipedia.PDCA](https://en.wikipedia.org/wiki/PDCA)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `TQM`

---

+ 1970s，美国国防部曾专门研究软件工程做不好的原因，发现70%的失败项目是因为管理存在的瑕疵引起的，而非技术性原因，从而得出一个结论，即管理是影响软件研发项目全局的因素，而技术只影响局部
+ `TQM`指在社会全面的推动下
    1. 企业中所有部门、所有组织、所有人员都 **_以产品质量_** 为核心
    1. 把专业技术、管理技术、数理统计技术集合在一起
    1. 建立起一套科学、严密、高效的质量保证体系
    1. 控制生产过程中影响质量的因素
    1. 以优质的工作、最经济的办法提供满足用户需要的产品的全部活动

---

![height:600](./assets_image/TQM.png)

---

### 质量管理的发展阶段

---

### `TQM`的含义

1. 强烈关注客户
1. 精确度量
1. 坚持不断地改进
1. 向员工授权
1. 改进组织的每项工作质量

---

![height:600](./assets_image/TQM_elements.png)

<!--FIXME: find the differences
---

### `TQM`相关问题

+ `美式TQM`：
    1. 全员参与
    1. 关注客户
    1. 持续改进
+ `TQM`与`TCS(Total Customer Satisfaction, 客户完全满意)`
-->

---

![height:500](./assets_image/PDCA_TCS.png)

---

### `TQM`与`ISO 9000`的相同点

1. 管理理论和统计理论基础一致
    1. 认为产品质量形成于产品全过程
    1. 要求质量体系贯穿于质量形成全过程
    1. 实现方法上均采用`PDCA`模式
1. 要求对质量实施系统化的管理
1. 强调高层管理者对质量的管理
1. 强调任一过程都可以不断改进和完善
1. 为提高产品质量、满足客户需要的目的一致

---

### `TQM`与`ISO 9000`的不同点

1. 期间目标不相同
    1. `TQM`的期间目标是改变现状，管理活动不重复进行
    1. `ISO 9000`的期间目标是维持标准现状，目标值是定值，管理活动是重复进行
1. 工作中心不相同
    1. `TQM`以人为中心
    1. `ISO 9000`以标准为中心

---

1. 执行标准不相同
    1. `TQM`： 企业结合自身特点制定的自我约束的管理体制
    1. `ISO 9000`： 国际公认的质量管理体系标准、世界各国共同遵守的准则
1. 检查方式不相同
    1. `TQM`： 检查方是企业内部人员，检查方式是考核和评价
    1. `ISO 9000`： 检查方是认证机构，检查方式是监督和检查

---

### `TQM`与统计技术

1. 从统计质量控制到全面质量管理
1. 统计技术是`TQM`的核心，是实现`TQM`的有效工具

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Six-Sigma`

---

1. 1985年由`Motorola`的`Bill Smith`提出
1. `Motorola`的`Graig Fullerton`提出了设计方法： `SSDM`/`DFSS`
1. 1988年赢得马可姆·波里奇奖（Malcolm Baldrige National Quality Award）

---

### `Six-Sigma`与`Zero-Bug`

+ 零缺陷（Zero-Bug）的概念是由被誉为“全球质量管理大师”、“零缺陷之父”和“伟大的管理思想家”的菲利浦•克劳士比（Philip B. Crosby）在20世纪60年代初提出的，并由此在美国推行零缺陷运动。
+ 零缺陷的思想传至日本，在日本制造业中得到了全面推广，使日本制造业的产品质量得到迅速提高，并且领先于世界水平，继而进一步扩大到工商业所有领域。

---

![height:600](./assets_image/what_is_six-sigma_01.png)

---

![height:600](./assets_image/what_is_six-sigma_02.png)

---

### `Six-Sigma`的特征

1. 以顾客为关注焦点
1. 提高顾客满意度和降低资源成本，来促使组织的业绩提升
1. 注重数据和事实，使管理成为基于数字的科学
1. 以项目为驱动
1. 实现对产品和流程的突破性质量改进
1. 有预见的积极管理
1. 无边界合作
1. 追求完美并容忍失误
1. 强调骨干队伍的建设
1. 遵循`DMAIC`的改进方法

---

### `Six-Sigma`的优点

1. 提升企业管理的能力
1. 节约企业运营成本
1. 增加顾客价值
1. 改进服务水平
1. 形成积极向上的企业文化

---

### `DPMO`与`Six-Sigma`

---

![height:600](./assets_image/DMPO.png)

---

![height:500](./assets_image/DPMO_sigma_level_table.png)

---

![height:600](./assets_image/six-sigma.png)

---

### `Six-Sigma`的人员组织结构

1. `Six-Sigma`管理委员会
1. 执行负责人
1. 黑带
1. 黑带大师
1. 绿带

---

![height:500](./assets_image/six-sigma_staff_organization.png)

---

### `TQM` vs `Six-Sigma`

1. `Six-Sigma`强调对关键业务流程的突破性改进，而不是一时一点的局部改进
1. `Six-Sigma`的开展依赖于高层管理者的重视，依靠高层领导人和决策者的自觉行动
1. `Six-Sigma`是一个具有挑战性的目标，在每百万次中仅出现3.4个缺陷
1. `Six-Sigma`强调客户驱动，是一种由客户驱动的管理哲学
1. `Six-Sigma`充分体现了跨部门的团队协作
1. `Six-Sigma`关注产生结果的关键因素

---

+ `Six-Sigma`突破了`TQM`的不足，强调从上而下的全员参与，人人有责，不留死角
+ :star2: 从某种意义上，**`Six-Sigma`是`TQM`的继承和发展**

---

### `BPR(Business Process Re-engineering, 企业流程再造)`与`Six-Sigma`

+ `BPR`强调以业务流程为改造对象和中心，以关注客户的需求和满意为目标
+ `BPR`主要利用最佳的管理实践对企业进行快速的改造，是推倒重来的方式 ==> 比较难于被组织心甘情愿地全面接受，适合于比较年轻的、管理还没有定型的企业
+ `Six-Sigma`是流程持续改进的方法，`Six-Sigma`由企业内部人员推动完成，需要的时间比较长，但改造比较彻底，企业内部员工有机会成为流程改进方面的专家，因而有持续改进的力量
+ 在实际项目中，`Six-Sigma`和`BPR`经常结合使用

---

|BPR方法|6σ管理方法|
|--|--|
|忽略分析|重视分析|
|推倒流程，重新再来|持续改进流程|
|缺乏衡量标准|完全量化|
|改进依赖于外部咨询师的建议|改进企业内部人员来推动完成|
|:star2: 员工参与少|:star2: 全员参与|
|实施时间短，奏效快|实施时间较长|
|适用于未定性的年轻企业|适用于定义了核心业务流程的企业|

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `DFSS`流程及其主要设计工具

---

### `DMAIC`与`DFSS`

+ 实施`Six-Sigma`提升企业竞争力的两个主要途径
    1. `DMAIC(Define, Measure, Analyze, Improve and Control))`: 现有业务流程改进遵循5步循环改进法
    1. `DFSS(Design For Six Sigma)`: 新过程、新产品设计

---

![height:600](./assets_image/DMAIC.png)

---

![height:600](./assets_image/procss_map_to_a_successful_DMAIC_project.png)

---

#### `DFSS`

DFSS是独立与传统六西格玛DMAIC的又一个方法论，以顾客需求为导向，以质量功能展开为纽带，深入分析和展开顾客需求，综合应用系统设计、参数设计、容差设计、实验设计以及普氏矩阵、失效模式与影响分析（Failure Mode and Effects Analysis，FMEA）等设计分析技术，大跨度地提高产品的固有质量，从而更好地满足顾客的需求

---

### `DFSS`的重要性及其内涵

+ 质量管理大师约瑟夫·朱兰（Joseph M. Juran）说过：在制造阶段所产生的任何缺陷在产品设计阶段都可以直接控制。
    1. 质量保证的措施首先要集中在设计过程上，目的在于一开始就避免存在某些缺陷。如果设计能力不足，所有的改进与控制都无从谈起。
+ 研究表明，由于设计所引起的质量问题至少是80%，即至少80%的质量问题源于劣质的设计，由此可见设计质量之重要

---

### `DFSS`的思想

+ 基于预防性思想
+ 基于并行质量工程的思想
+ 以客户为关注焦点

---

### `DFSS` vs `DMAIC`

区分DMAIC和DFSS的方法是通过确定6σ行为发生在产品生命周期的什么阶段以及其着重点。
一方面，DAMIC侧重于主动找出问题的起因和源头，从根本上解决问题，强调对现有流程的改进，
但该方法并不注重产品或流程的初始设计，即针对产品和流程的缺陷采取纠正措施，通过不断改进，使流程趋于“完美”。
但是，通过DMAIC对流程的改进是有限的，即使发挥DMAIC方法的最大潜力，产品的质量也不会超过设计的固有质量。
相应地，DMAIC重视的是改进，对新产品几乎毫无用处，因为新产品需要改进的缺陷还没有出现。

---

### `DFSS`流程

![height:450](./assets_image/DFSS_methodology.png)

---

### `DFSS`设计工具

---

![height:600](./assets_image/DFSS_toolkit.png)

---

![height:600](./assets_image/DFSS_design_process_and_toolkit.png)

---

#### `DDOVP(Design, Design, Optimize, Validate and Plan)`

1. 定义（Define）： 定义和开发需求（Define and Develop Requirements）
1. 设计（Design）： 并行产品和过程设计（Concurrent Design）
1. 验证（Validate）： 优化（Optimize）
1. 评价和验证（Evaluate and Validate）
1. 计划（Plan）： 开发控制计划（Develop Control Plan）

---

1. 设计路线注重的是顾客需求和设计优化的重要性，更适于在产品开发阶段应用
1. 建立和保持一个高度有效的并行工程团队是实现DFSS的前提

---

### `DFSS`的集成框架

---

![height:600](./assets_image/DFSS_integrated_framework.png)

---

### 实施`Six-Sigma`的注意事项

1. 机械模仿
1. 缺乏建立`Six-Sigma`持续改进的质量文化
1. 没有对`Six-Sigma`的专业培训和咨询
1. 基础管理相对薄弱
1. 缺乏科学合理的项目实施规划

---

### DFSS的发展方向

1. 支持DFSS的软件工具平台的开发
1. `Six-Sigma`设计的管理问题
1. 将顾客需求转换为可量化的设计特性和关键过程
1. 概念设计中的解耦合设计
1. 稳健性设计技术的深入研究

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `QFD(Quality Function Deployment, 质量功能展开)`

---

+ `QFD`是一种客户驱动的产品系统设计方法与工具，代表从传统设计方式（“设计 -> 试验 -> 调整”）向现代设计方式（主动、预防）的转变
+ `QFD`是系统工程思想在产品设计和开发过程中的具体运用
+ `QFD`于1970s初起源于日本，并被三菱重工神户造船厂成功地应用于船舶设计与制造中
+ 丰田公司于1970s后期使用这个方法，取得了巨大的经济效益，新产品开发启动成本下降了61%，产品开发周期缩短了1/3，而质量也得到了改进
+ 1980s中期，`QFD`被介绍到欧美，迅速引起了学术界和企业界的研究和应用
+ `QFD`不仅应用于制造业，还应用于计算机软件业以及企业的战略规划等领域中

---

+ `ASI(American Supplier Institute, 美国供应商协会)`对`QFD`的定义： 指在制造过程中，用系统配置需求和特征关系的方法将顾客需求转变成“质量特性”并展开质量设计，最终得到满足质量要求的产品
+ `QFD`从客户需求出发，确定各个功能部件的质量，然后将分解到每个零部件和加工过程的质量进行展开，通过分解关系网络，组成制造过程的整体质量
+ `QFD`的每个分解阶段，都产生一个质量屋，上一层的输出，恰好是下一层的输入，形成瀑布式的分解过程

---

![height:520](./assets_image/QFD_decomposition_model.png)

---

![height:520](./assets_image/house_of_quality_structure.png)

---

### `QFD`的特点

+ 质量功能展开的整个过程是以满足顾客需求为出发，各阶段的质量屋输入和输出为市场顾客需求驱动的，以此来保证最大限度地满足顾客需求。
+ 质量功能展开技术在计算机技术和信息技术的支持下，有机地继承和延伸传统设计技术方法，是传统的理论方法在一个新的层次上应用和发展。同时还可以和其它先进设计技术方法融合应用。
+ 质量屋是建立质量功能展开系统的基础工具，是质量功能展开方法的精髓。
+ 在质量功能展开系统化过程中的各个阶段，我们都要将市场顾客需求转化为管理者和产品设计者能明确理解的各种信息，并减少产品设计过程的盲目性。
    1. 从工程设计角度看，这种有目标有计划的开发生产模式可以降低设计费用、缩短开发周期，最后大大提高产品的质量和竞争能力。

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Slide :ok:
