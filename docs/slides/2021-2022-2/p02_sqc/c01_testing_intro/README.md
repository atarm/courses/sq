---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Introduction to Software Testing_"
footer: "@aRoming"
math: katex
---

<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Introduction to Software Testing

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [⭐ 软件测试的定义、目的和原则Definition, Purposes and Principles of Software Testing](#软件测试的定义-目的和原则definition-purposes-and-principles-of-software-testing)
2. [🌟 测试用例Test Case](#测试用例test-case)
3. [🌟 软件测试的类型The Types of Software Testing](#软件测试的类型the-types-of-software-testing)
4. [🌟 测试阶段与V模型Testing Phases and V Model](#测试阶段与v模型testing-phases-and-v-model)
5. [敏捷测试Agile Testing](#敏捷测试agile-testing)
6. [测试工具Testing Tools](#测试工具testing-tools)
7. [软件测试的过去、现在和将来The Past, Present and Future of Software Testing](#软件测试的过去-现在和将来the-past-present-and-future-of-software-testing)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## ⭐ 软件测试的定义、目的和原则Definition, Purposes and Principles of Software Testing

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 软件测试是什么What is Software Testing

---

1. 1957年之前：**_调试导向(Debugging Oriented)/没有测试_**
1. 1957–1978：**_证明导向(Demonstration Oriented)/验真_**
    1. 1975年，测试先驱John Good Enough和Susan Cerhart给出了软件测试的通用定义"证明软件的工作是正确"的活动
    1. 1983 _IEEE_: 软件测试是使用人工或自动手段来运行或测定某个系统的过程，检验它是否满足规定的需求或者弄清预期结果与实际结果之间的差别， **软件测试以检验是否满足需求为目标**

---

1. 1979–1982：**_破坏导向(Destruction Oriented)/找错_**
    1. 1979 G.J. Myers _"The Art of Software Testing"_： **软件测试是为了发现软件错误而执行的过程（找错）**
        - 测试是程序的执行过程，目的在于发现错误
        - 一个好的测试用例可以发现至今尚未发现的错误
        - 一个成功的测试能发现至今未发现的错误

---

1. 1983–1987：评估导向(Evaluation Oriented)、度量
    1. 1983年， Bill Hetzel 著作的《软件测试完全指南》指出"测试是以评价一个程序或者系统属性为目标的任何一种活动，测试是对软件质量的度量"，开启了软件测试不仅仅是证明软件是对或不对的，而应该进行缺陷预防
1. 1988–至今：预防导向(Prevention Oriented)
    1. 2002年，Rick 和 Setfan在《系统的软件测试》一书中进一步对软件测试定义"测试是为了度量和提高被测软件的质量，对测试软件进行工程设计、实施和维护的整个生命周期"，该定义进一步丰富了软件测试的内容

🖊️ 证明者==>破坏者==>评估者==>提升者 🖊️

---

>1. 验证软件是否满足软件开发合同或项目开发计划、系统/子系统设计文档、软件需求规格说明、软件设计说明和软件产品说明等规定的软件质量要求
>1. 通过测试，发现软件缺陷
>1. 为软件产品的质量测量和评价提供依据
>
>>GB/T 15532-2008 计算机软件测试规范 - 软件测试的目的

---

>测试不能提升软件质量，但没有测试也不可能得到高质量的软件

>Inspection does not improve the quality, nor guarantee quality. Inspection is too late. The quality, good or bad, is already in the product.
>
>>[Inspection is too late. The quality, good or bad, is already in the product.](https://deming.org/inspection-is-too-late-the-quality-good-or-bad-is-already-in-the-product/#:~:text=Inspection%20is%20too%20late.,Edwards%20Deming%20Institute)

<!--

---

### 软件测试的目的Goals of Software Testing

1. 表面上，`SE`的其他阶段是建设性的，而`ST`是破坏性的
1. 实质上，`ST`的目的也是建立高质量的软件产品

>Software Testing is the process of executing a program or system with the intent of finding errors.  
>软件测试就是为了发现缺陷而运行程序的过程  
>--G. J. Myers

-->

<!--

---

### 软件测试的作用The Effects of Software Testing

1. 专业作用：从开发独立出来专门从事软件测试 ❓ ==> 与开发技能相补充的从事测试活动的技能
1. 角色作用：与开发相异的独立角色定位，独立承担软件成果质检的职责 ❓ ==> 与实现业务需求的开发角色相协作的测试角色（承担软件成果质检需求的测试角色或承担测试服务的测试开发角色）
1. 流程作用：确保`SQ`合格（验证产品是否合格）的重要手段之一

❗ 角色 != 具体的某个人 ❗

-->

<!--

---

### 软件测试的工作内容What do Software Testing Do

1. 广义的`ST`包括`SDLC`内所有的检查、评审、验证和确认活动
1. 狭义的`ST`指对程序进行验证的活动

🖊️

1. 确认validation ==> do right thing
1. 验证verification ==> do thing right

-->
---

### 软件测试的原则Principles of Software Testing

1. 🌟 穷举测试是不可能的
1. 🌟 对合理的和不合理的输入数据都要进行测试
1. 🌟 重点测试可能存在错误群集的程序区域
1. 🌟 长期完整保留所有的测试用例和测试文档 <!--直到该软件产品被废弃为止（⚠️ :exclamation: 书上该点有误，即然软件产品被废弃也应该保留 :exclamation: ⚠️ ）-->
1. 尽早地进行测试（测试左移）
1. 持续地进行测试

---

1. 不应假想程序不存在错误
1. 设计测试用例时，给出测试用例的预期结果
1. 除检查程序功能是否完备外，还要检查程序功能是否存在多余
1. 测试工作应避免由`developer`或开发组织本身来承担（⚠️ ❗ 书上该点有误，该观点不正确 ❗ ⚠️ ）

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 🌟 测试用例Test Case

---

### 🌟 测试用例是什么What is Test Case

In software engineering, a test case is _**a specification**_ of the _**inputs输入**_, _**execution conditions执行条件/前提条件**_, _**testing procedure测试步骤**_, and _**expected results预期输出**_ that define a single test to be executed to achieve a particular software testing objective, such as to exercise a particular program path or to verify compliance with a specific requirement.

>["Systems and software engineering - Vocabulary". Iso/Iec/IEEE 24765:2010(E). 2010-12-01. pp. 368.](https://ieeexplore.ieee.org/document/5733835)

---

### 测试用例的基本原则Essential Principles of Test Case

1. 🌟 可测试性：一个测试用例的预期输出必须是可以检验的，可以根据相关开发文档（或推理）得到明确的、可判定的结论
1. 🌟 可重现性：对于相同的测试用例，系统的预期执行结果应该完全相同，否则，如果系统预期输出存在不确定性，一旦实际运行该测试用例，也无法进行校验
1. ⭐ 独立性：测试用例应尽量相互独立（正交特性）
1. 典型性：能揭示最有可能存在缺陷的地方，能代表和覆盖合理与不合理、合法或不合法的情况

<!--TODO:

## 软件测试活动及其产出物The Activities and Its Outputs of Software Testing

---

><https://www.jianshu.com/p/66770ff7ee58>

参考此文档重新绘制软件测试的活动及每个活动的产出物

-->

---

### `GWT`测试用例描述结构`Given-When-Then (GWT)` Test Case Description Structure

- Given-When-Then (GWT) is a semi-structured way to write down test cases. They can either be tested manually or automated as browser tests with Selenium.

>[Given-When-Then](https://en.wikipedia.org/wiki/Given-When-Then#:~:text=Given%2DWhen%2DThen%20(GWT,words%20given%2C%20when%20and%20then.)

---

- `given`
    - `pre-conditions`: {job_status: "scheduled"}
    - `fixed data`: {job_number: 1255, job_scheduled_date: 2022-10-24}
- `when`
    - `action`: {"request to cancel a job"}
    - `input data`: {request_date: 2022-10-22}
- `then`
    - `output data`: {"your job has canceled, would you like to reschedule for a future date?"}
    - `post-conditions`: {job_status: "canceled"}

---

- the `Given, When, Then` format is often used when writing scenarios for automated acceptance tests. Cucumber enforces this format

```txt
GIVEN a registered user 'bob'
THEN a user navigates to the Sign In page
THEN the user signs in as 'bob'
THEN the profile page for 'bob' is displayed
```

>[Perryn Fowler. Given, When, Then and how not to do it.](https://archive.ph/20140814232159/http://www.jroller.com/perryn/entry/given_when_then_and_how)

---

1. Given-When-Then (GWT) is a structured format for expressing scenarios with example data, including pre- and post-conditions.
1. GWT helps project stakeholders (business, customer and technology partners) communicate using business domain language.

>[Using "Given-When-Then" to Discover and Validate Requirements](https://www.ebgconsulting.com/blog/using-given-when-then-to-discover-and-validate-requirements-2/)

---

- story: as a customer, i want to cancel a window, so that i can cleaning job
- scenario: customer contacts Squeeky Kleen two business days before the job's scheduled date
- business rule: a job canceled less than one business day before the job's scheduled date must be charged a cancellation fee

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 🌟 软件测试的类型The Types of Software Testing

<!--

---

### 5要素和3资源

1. 5要素（essential element）： 质量/质量标准、人员、技术、资源和流程
1. 3资源/信息：
    1. 软件配置： `SRS(Software Requirement Specification)`, `SDD(Software Design Description)` , `software configuration data` , `src`等
    1. 测试配置： `testing scheme测试方案`、`test case测试用例`和`test driver测试驱动程序`等
    1. 测试工具： `CATT(Computer Aided Testing Tool)`等

-->

---

<!--

### 软件测试的分类The Classifies of Software Testing

-->

<!--TODO:-->

1. 按 **是否运行程序** 划分：`static testing`, `dynamic testing`
1. 按 **测试执行者** 划分：`manual testing`, `automated testing`
1. 按 **测试用例的设计方法** 划分：`black-box testing`, `white-box testing`
1. 按 **测试阶段/测试级别** 划分：`unit testing`, `integration testing`, `system testing`, `acceptance testing`/`requirement testing`(`alpha testing`, `beta testing`), `regression testing`
1. 按 **质量属性** 划分：`function testing`, `performance testing`...
1. 其它维度划分
<!--1. 按 **测试目的** 划分：,`function testing`, `performance testing`, `stress testing`, `alpha testing`, `beta testing`, `...`-->

---

>1. [软件测试活动分类：按测试阶段分类](https://mp.weixin.qq.com/s?__biz=MzI2NDk1NTU4Ng==&mid=2247484531&idx=1&sn=fcdee0af85904610f5dfff6737924a7b&chksm=eaa5f7deddd27ec85bec350aff8d34af6d8834f9dd2c12f14f11aceebffdd8a209b1efe0b26d&token=1387502905&lang=zh_CN&scene=21#wechat_redirect)
>1. [软件测试活动分类：按质量属性分类](https://mp.weixin.qq.com/s?__biz=MzI2NDk1NTU4Ng==&mid=2247484524&idx=1&sn=cd25ec23851756782cf3febc1c06c113&chksm=eaa5f7c1ddd27ed7fd79ff11d489b002bb23a6463a984da7ff1e9304f407041647f8b119c2c0&token=1387502905&lang=zh_CN&scene=21#wechat_redirect)
>1. [软件测试活动分类：按测试覆盖分类](https://mp.weixin.qq.com/s?__biz=MzI2NDk1NTU4Ng==&mid=2247484514&idx=1&sn=3ee433ab7014975d6356166b1de9a2a5&chksm=eaa5f7cfddd27ed9a3de318e4aa6c3f03956e94cb0a8e4b7a02ea23c4fc15a66af7e8b56a298&token=1387502905&lang=zh_CN&scene=21#wechat_redirect)
>1. [软件测试活动分类：按测试方法分类](https://mp.weixin.qq.com/s?__biz=MzI2NDk1NTU4Ng==&mid=2247484523&idx=1&sn=fb2acba0279318e2b47a5e41176bbddf&chksm=eaa5f7c6ddd27ed0fe0751553c119b358c21bd7c5bcf46d7697c8ae847dea22a89809feb8827&token=1387502905&lang=zh_CN&scene=21#wechat_redirect)

<!--TODO:
1. 是否存在grey-box testing？
1. 是否能按测试目的划分？
-->

<!--
回归测试不应当归类到按阶段划分的类型中

1. `regression testing回归测试`： 回归测试指修改软件后重新进行的测试，该测试用以验证修改后是否引入新缺陷或导致其他相关组件产生缺陷
-->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 🌟 测试阶段与V模型Testing Phases and V Model

---

### 测试阶段

1. `unit testing单元测试`： 对软件中的最小可测试单元或基本组成单元进行测试
1. `integrated testing集成测试`： 将已通过单元测试的模块按照设计的要求组装成子系统或系统，然后进行测试
1. `system testing系统测试`： 将已通过集成测试的软件作为整个系统的一部分与运行硬件、外部软硬件、人员等系统元素结合在一起，在实际（或模拟实际）的使用环境下进行测试，以保证系统交付给用户之后能正常使用
1. `acceptance testing验收测试`： 以用户为主，`developer`、`operator`、`SQA`等共同参与的测试，验收测试让用户决定是否接收产品

---

|开发阶段|测试活动|
|--|--|
|需求|1. 确定测试步骤<br/>2. 确定需求是否恰当<br/>3. 生成功能测试用例<br/>4. 确定设计是否符合需求|
|设计|1. 确定设计信息是否足够<br/>2. 准备结构和功能的测试用例<br/>3. 确定设计的一致性|
|编码|1. 为单元测试产生结构和功能测试的测试用例<br/>2. 进行足够的单元测试|

---

|开发阶段|测试活动|
|--|--|
|测试|从功能和非功能需求角度测试系统|
|验收/发布|从用户需求角度测试系统|
|安装|把测试过的系统投入生产|
|维护|修改缺陷并重新测试|

---

### V模型V Model

<!--

---

### 测试阶段与开发阶段关系的发展

1. 传统的软件工程观点： `testing`和`develop`是两个目的不同的、独立的阶段 ==> 狭义的`testing`
1. 现代的软件工程观点： `testing`应贯穿于整个`SDLC` ==> 广义的`testing`

-->

>1. [Using V Models for Testing](https://insights.sei.cmu.edu/blog/using-v-models-for-testing/)
>1. [Systems Engineering and ITS Project Development](https://ops.fhwa.dot.gov/plan4ops/sys_engineering.htm)
>1. [What is V Model in software testing and what are advantages and disadvantages of V Model](https://testingfreak.com/v-model-software-testing-advantages-disadvantages-v-model/)
>1. [V-Model (software development)](https://en.wikipedia.org/wiki/V-Model_%28software_development%29)

---

![height:560](./.assets/image/v_model.png)

---

![height:500](./.assets/image/vmodel.png)

>[What is V Model in software testing and what are advantages and disadvantages of V Model](https://testingfreak.com/v-model-software-testing-advantages-disadvantages-v-model/)

---

![height:520](./.assets/image/sys_eng_vmodel.png)

>[Systems Engineering and ITS Project Development](https://ops.fhwa.dot.gov/plan4ops/sys_engineering.htm)

---

#### The Traditional V Model and Three Variants from Tester's View

>[Using V Models for Testing](https://insights.sei.cmu.edu/blog/using-v-models-for-testing/)

---

##### Traditional V Model

![height:520](./.assets/image/F1_-_Traditional_V_Model.original.jpg)

---

##### Tester's Single V Model of Testable Work Products

![height:520](./.assets/image/F2_-_Executable_Work_Product_V_Model.original.jpg)

---

##### Tester's Double V Model of Testable Work Products and Corresponding Tests

![height:500](./.assets/image/F3_-_Double_V_Model.original.jpg)

---

##### The Tester's Triple V Model of Work Products, Tests, and Test Verification

![height:500](./.assets/image/F4_-_Triple_V_Model.original.jpg)

---

1. The single V model: modifies the nodes of the traditional V model to **_represent the executable work products to be tested_** rather than the activities used to produce them.
1. The double V model: adds a second V to **_show the type of tests corresponding to each of these executable work products_**.
1. The triple V model: adds a third V to illustrate the importance of verifying the tests to determine whether they contain defects that could stop or delay testing or lead to false positive or false negative test results.

---

#### V模型的价值

1. 表明各测试阶段对应的开发阶段及其参照标准
1. 表明各测试阶段的测试计划应与对应的开发阶段同步进行
1. `double V model`和`triple V model`表明各开发阶段的非代码产出物应立应同步进行测试（`TDD`）
1. `triple V model`引入了对测试本身的`verification`

⚠️ 未找出V模型由什么人、在什么时候、什么场景下提出 ⚠️

---

### 单元测试Unit Testing

---

#### 单元测试的测试内容

1. 接口测试
1. 局部数据结构测试
1. 重要执行路径测试
1. 边界条件测试
1. 错误处理测试

---

#### 单元测试的测试方法

![height:520](./.assets/image/the_structure_of_unit_testing.png)

---

#### 单元测试的测试技术

1. 静态测试：静态代码分析（白盒）
1. 动态测试：白盒为主、黑盒为辅

<!--TODO:
1. 功能测试和非功能测试（⚠️ :exclamation: 书上该点有误，这不属于测试技术 :exclamation: ⚠️）
-->

---

#### 单元测试的测试人员

1. 开发人员
1. 特殊情况下，可能会邀请用户代表参与

---

### 集成测试Integrated Testing

---

#### 集成测试的测试内容

1. 接口：将各模块连接起来时穿越模块接口的数据是否会丢失
1. 功能：
    1. 各子功能组合起来能否达到预期要求
    1. 模块的功能是否会对其他模块的功能产生不利影响
1. 全局数据结构：全局数据结构是否有问题，是否会被异常修改
1. 误差累积：单个模块的误差累积起来是否会放大到不可接受的程度

---

#### 集成测试的测试方法

1. 非增量式集成测试方法
1. 增量式集成测试方法
    1. 自顶向下增量式集成测试
    1. 自底向上增量式集成测试

---

#### 自顶向下与自底向上TtoB vs BtoT

||Advantage|Disadvantage|
|--|--|--|
|TtoB|1. 易于排查顶端错误<br/>2. 较早出现程序轮廓<br/>3. 加入`IO`模块后，较方便描述测试用例|1. `stub`较难设计<br/>2. 模块介入使结果较难观察
|BtoT|1. 易于排查底端错误<br/>2. 易于产生测试条件和观察测试结果<br/>3. 易于编写`driver`|1. 程序轮廓较晚出现<br/>2. 必须给出`driver`

---

#### 集成测试的测试技术

黑盒测试为主、白盒测试为辅

---

#### 集成测试的测试人员

1. 开发人员、开发组的测试人员或独立的测试小组
1. 独立的测试观察员
1. 必要时，可能会邀请用户代表参与

---

### 系统测试System Testing

---

#### 系统测试的测试内容

1. 功能测试
1. 非功能测试： 性能测试、强度测试/压力测试、可靠性测试、故障恢复测试、安全性测试、配置测试、可用性测试、兼容性测试、可安装性测试等等

---

#### 系统测试的测试技术

一般采用黑盒测试技术

---

#### 系统测试的测试人员

1. 独立的测试小组
1. 独立的测试观察员
1. 允许时，应该邀请用户代表参与

---

### 验收测试Acceptance Testing

---

#### 验收测试的测试内容

1. 功能测试
1. 非功能测试
1. 用户需求要求的其他测试

---

#### 验收测试的测试技术

1. 完全采用黑盒测试技术
1. 可采用`alpha test`/内部测试、`beta test`/公众测试

---

#### 验收测试的测试人员

1. 用户代表
1. 独立的测试小组
1. 独立的测试观察员

---

### 回归测试Regression Testing

---

#### 回归测试的测试策略

1. 测试用例库的维护
1. 回归测试包的选择
    1. 重新测试全部测试用例
    1. 基本风险选择测试用例
    1. 基于操作剖面选择测试用例
    1. 测试修改部分对应测试用例

---

#### 回归测试的测试过程

1. 识别软件中修改的部分
1. 建立新的测试用例库： 去掉不再适用的
1. 依据策略选择测试用例进行测试
1. 如有必要
    1. 增加新的测试用例至测试用例库
    1. 依据策略选择测试用例进行测试

---

#### 回归测试的测试技术

1. 黑盒测试
1. 白盒测试
1. 错误预测

---

#### 回归测试的测试人员

根据不同的测试阶段对应的人员

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 敏捷测试Agile Testing

---

### 敏捷Agile

1. 敏捷开发： 高度迭代、周期性，达到及时、持续地响应客户需求的频繁反馈的效果
1. 敏捷测试： 不断修正质量指标，正确建立测试策略，确认客户的有效需求得以圆满实现和确保整个生产的过程安全的、及时的发布最终产品

---

### 敏捷测试过程

![height:520](./.assets/image/the_agile_testing_process.png)

---

### 测试驱动开发Test-Driven Development

`TDD(Test-Driven Development, 测试驱动开发)`： 是一种软件开发过程中的应用方法，由`XP(eXtreme Programming, 极限编程)`中倡导，`TDD`倡导先写测试程序，然后再编码实现其功能

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 测试工具Testing Tools

---

### 测试工具的作用

1. 提高测试效率
1. 对新版本进行回归测试
1. 执行更多更频繁的测试
1. 执行一些手工测试困难或不可能做的测试
1. 更好地利用资源
1. 提高测试的一致性和可重复性
1. 提高测试的复用性
1. 增加软件信任度
1. 更快地将软件推向市场

---

### 测试工具的类型

1. 白盒测试工具
1. 黑盒测试工具
1. 测试设计与测试开发工具
    1. 测试数据生成器
    1. 基于需求的测试设计工具
1. 测试执行与测试评估工具
    1. 捕获/回放工具
    1. 覆盖分析工具
    1. 内存测试工具
1. 测试管理工具

---

### 测试工具的选择关注点

1. 功能
    1. 测试功能
    1. 报表功能
    1. 集成与兼容能力
1. 成本
    1. 购买成本
    1. 学习成本
    1. 使用成本
    1. 保值成本

<!---

### 白盒测试工具

1. 静态测试工具： `Checkstyle`, `FindBugs`, `PMD`, `Parasoft Jtest`
1. 动态测试工具： `Compuware DevPartner`, `IBM Rational Purify`

-->

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 软件测试的过去、现在和将来The Past, Present and Future of Software Testing

---

1. 过去（约2000以前）
    1. 约1957：从`debug`独立出`testing`
    1. 约2000前：从人工测试到半自动化测试
1. 现在（约2000 – 现在）
    1. 测试的比重增加： 需求分析约占 _3%(9.09%)_，设计约占 _5%(15.15%)_，编码约占 _7%(21.21%)_，测试约占 _15%(45.45%)_，部署及运维约占 _67%(203.03%)_
    1. 从半自动化测试到自动化测试
1. 将来（现在 – 未来）：自动化程度越来越高、代码缺陷预测能力越来越强

---

1. 软件测试重要性和规范性不断提高
1. 从手工测试向自动化测试方式的转变
1. 测试服务体系初步形成
1. 测试人员需求逐步增大，素质不断提高
    - 测试人员应具备工程化编码能力
    - 开发人员承担低阶测试任务

---

### 自动化测试

---

1. 自动化测试是趋势（自动化运维是另一个趋势）
1. 自动化测试的核心是`testing script`
1. 自动化测试不代表完全不需要手工测试

---

#### 自动化测试的原因

1. 提高效率、提高准确率
1. 降低重复性劳动、降低成本
1. 避免人为因素引入的测试错误
1. 某些场景人工无法进行有效测试

---

#### 自动化测试的类型

1. 录制脚本测试<!--/线性脚本测试-->
    1. 录制 -> 回放
    1. 录制 -> 编辑 -> 回放
1. 编程脚本测试：
    1. 基本脚本测试
    1. 进阶脚本测试
        1. 数据驱动测试：数据与算法（步骤）相分离

---

#### 适合引入自动化测试的条件

1. 具有良好定义的测试策略和测试计划
1. 对于自动化测试，拥有一个能够被识别的测试框架
1. 能够确保多个测试运行的构建策略
1. 多平台环境需要测试
1. 拥有运行测试的硬件
1. 拥有关注在自动化过程上的资源

---

#### 不适合引入自动化测试的条件

1. 组织或项目没有标准的测试流程
1. 项目没有清晰的测试计划/测试蓝图（测试什么、什么时候测试）
1. 在一个项目中，测试责任人是一个新人，并且还不是完全的理解方案的功能性或者设计
1. 整个项目在时间的压力下且没有自动化测试经验
1. 团队中没有资源或者具有自动化测试技能的人

---

#### 不同阶段的自动化测试策略

|测试阶段|策略|
|--|--|
|单元测试|自动化测试|
|集成测试|尽可能自动化测试|
|系统测试|人工测试 + 自动化测试（如：性能类测试等）|
|验收测试|人工测试为主|

---

### SWE, SET and TE

1. `SWE(Software Engineer)`
1. `SET(Software Engineer in Test)`
1. `TE(Test Engineer)`
1. `outsource TE`

>统计表明，包括BAT在内的部分IT公司逐步取消了测试工程师岗位，进而转变为招聘测试开发岗位。  
> [「测试开发工程师」和「软件测试工程师」有什么区别？](https://www.zhihu.com/question/19986024)

---

>「测试开发工程师」和「软件测试工程师」有什么区别？ - 飘哥的回答 - 知乎
<https://www.zhihu.com/question/19986024/answer/932983801>
>
>微软作为软件时代发迹的软件公司，对于软件开发有一套严格而切实有效的开发流程和管理模式方式。它在组织架构上是典型的开发，测试，和项目经理三权分立的架构。这三个组织的最高领导人，在很多的组织里，一直到VP以上才汇报给一个人。

---

>举例来说，在微软的在线服务部门，陆奇才是第一个让必应的开发，测试，和项目经理的最高负责人汇报到一起的人。微软之所以采取这个架构，在1995年的时候是可以理解的。那个时候互联网并不发达，软件开始主要靠软盘后来靠光盘发售。如果有严重的bug，修复起来的成本非常的高。所以在当时，稳定的开发和严格的测试，以及明确的用户需求都是一个软件公司成功必不可少的。

---

>但是到了互联网时代互联网公司不是这样的。Facebook创始人马克扎卡伯格提倡的是快速开发快速发布，有bug上线以后再修。测试在新兴的互联网企业里面的比重大幅度的下降。因为互联网公司的软件都是自己部署的，所以可以很方便的修改和重新部署，不需要测试的那么仔细。与此同时，现在互联网已经非常发达了，即使传统软件公司要修一个严重的bug并让所有买了软件的人更新，也不再是一件那么高成本的事情。

---

>所以微软养着的庞大的测试队伍，不但消耗微软大量的资源，而且不产生实际的效益。陆奇领导的在线服务部门。其改革的主要目的是为了大幅度减少这些并不产生实际效益的测试部门。陆奇效仿其他互联网公司，在Bing开发组织中推行所谓的Combined Engineering。具体来说是把开发人员和测试人员混编，大家都干同样的事情。这个Combined Engineering开始只是一两个小组试点。并到宣传这种做法的好处，开发效率高，开发人员对自己的代码负责等等。大约一年多以后整个必应都被Combine掉了，测试作为一个独立的组织在微软的在线服务部门不存在了。

---

>专门的测试人员还留了一点，大部分的和开发人员合并，并且汇报给同一个领导。合并以后测试人员以开发人员的标准做绩效考核的。结果就是测试人员的绩效考核通常都比较低，而那些垫底的就被开除了。微软的陆奇通过这个Combined Engineering，解决了很多微软高层一直不能解决的问题：裁减测试人员。这个Combined Engineering后来就大幅度向微软各个部门推广了。

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
