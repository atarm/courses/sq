# QA-E04：基于FitNesse的验收测试Acceptance Testing Based-On FitNesse

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [实验基本信息](#实验基本信息)
2. [实验前提条件与储备知识](#实验前提条件与储备知识)
3. [实验注意事项](#实验注意事项)
4. [实验指引](#实验指引)
    1. [实验内容01](#实验内容01)
        1. [**:microscope: 实验内容01具体任务**](#microscope-实验内容01具体任务)
        2. [**:ticket: 实验内容01参考截图**](#ticket-实验内容01参考截图)
        3. [:bookmark: 实验内容01相关知识](#bookmark-实验内容01相关知识)
    2. [实验内容02](#实验内容02)
        1. [**:microscope: 实验内容02具体任务**](#microscope-实验内容02具体任务)
        2. [**:ticket: 实验内容02参考截图**](#ticket-实验内容02参考截图)
        3. [:bookmark: 实验内容02相关知识](#bookmark-实验内容02相关知识)
    3. [实验内容03](#实验内容03)
        1. [**:microscope: 实验内容03具体任务**](#microscope-实验内容03具体任务)
        2. [**:ticket: 实验内容03参考截图**](#ticket-实验内容03参考截图)
        3. [:bookmark: 实验内容03相关知识](#bookmark-实验内容03相关知识)
5. [Futhermore](#futhermore)

<!-- /code_chunk_output -->

## 实验基本信息

1. 实验性质：设计性
1. 实验学时：4
1. 实验目的：
    1. 理解`验收测试（Acceptance Testing）`的相关概念与知识
    1. 理解`FitNeese`的使用方法
1. 实验内容与要求：
    1. 实验内容01： 安装并启动`FitNesse`
    1. 实验内容02： 使用`FitNesse`编写`Test Case`
    1. 实验内容03： 使用`FitNesse`编写`Test Suite`
1. 实验条件：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 软件环境：`Eclipse`或`Intellij IDEA`或`JDK`

## 实验前提条件与储备知识

1. 掌握`Java`基本编程
1. 了解`FitNeese`的基本概念
1. 了解`WikiWord`： `WikiWord`是由一个或多个首字母大写、单词组合在一起，这组语法也被称为`mixed case`或`camel case`，在`Wiki`系统中，`WikiWord`就是页面的链接名称

## 实验注意事项

1. 实验指引中使用尖括号`<>`表示键盘按键，如：`<CTRL>`代表control键，`<j>`代表j键
1. 实验中请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”
1. 实验内容与实验指引中各子内容包含三部分：
    1. **:microscope: 实验内容具体任务** ==> 实验的具体任务及步骤
    1. **:ticket: 实验内容参考截图** ==> 实验的参考结果截图
    1. **:bookmark: 实验内容相关知识** ==> 实验涉及的相关知识

## 实验指引

### 实验内容01

#### **:microscope: 实验内容01具体任务**

1. 通过`CLI`启动`FitNesse`：`java -jar fitnesse-standalone.jar -p 1234`
1. 在浏览器中打开`FitNesse`：输入网址`localhost:1234`

:warning: :warning: :warning: 新版`FitNesse`不支持`32-bit OS`，`20161106`版经测试支持`32-bit OS`，但`20161106`版不支持`JDK15`，建议使用`JDK8`:warning: :warning: :warning:

#### **:ticket: 实验内容01参考截图**

![](./assets_image/E0401_01.png)

![](./assets_image/E0401_02.png)

#### :bookmark: 实验内容01相关知识

### 实验内容02

#### **:microscope: 实验内容02具体任务**

1. 编辑`FrontPage`页面，新增一个测试项目`TestProject`页面
    1. `[[Test Project][.TestProject]]`
1. 创建刚刚定义的`TestProject`页面，并依次增加以下文本
    1. `!define TEST_SYSTEM {slim}`
    1. `!path C:\expt_ws\FitnesseTestProject\bin`
    1. `>TestCase001`
1. 创建刚刚定义的`TestCase001`页面，并依次输入以下文本
    1. `!define name {mark}`
    1. `!define result {say hello to mark}`
    1. `!|import|`
    1. `|com|`
    1. `!|script|TestFitnesse|`
    1. `|check|sayHelloTo|${name}|${result}|`
1. 在`TestProject`定义的`path`路径下新增一个`com.TestFitnesse`类并编译
1. 运行`TestCase001`

:warning: `!path`请根据实际环境定义 :warning:

#### **:ticket: 实验内容02参考截图**

![](./assets_image/E0402_01.png)

![](./assets_image/E0402_02.png)

![](./assets_image/E0402_03.png)

![](./assets_image/E0402_04.png)

![](./assets_image/E0402_05.png)

![](./assets_image/E0402_06.png)

![](./assets_image/E0402_07.png)

![](./assets_image/E0402_08.png)

![](./assets_image/E0402_09.png)

![](./assets_image/E0402_10.png)

![](./assets_image/E0402_11.png)

#### :bookmark: 实验内容02相关知识

暂无

### 实验内容03

#### **:microscope: 实验内容03具体任务**

1. 在`TestProject`页面中新增`>TestCase002`
1. 将`TestProject`的属性从`Test`修改成`Suite`
1. 创建刚刚定义的`TestCase002`页面，并依次输入以下文本
    1. `!define name {victor}`
    1. `!define result {say hello to mark}`
    1. `!|import|`
    1. `|com|`
    1. `!|script|TestFitnesse|`
    1. `|check|sayHelloTo|${name}|${result}|`
1. 运行`TestProject`

#### **:ticket: 实验内容03参考截图**

![](./assets_image/E0403_01.png)

![](./assets_image/E0403_02.png)

![](./assets_image/E0403_03.png)

![](./assets_image/E0403_04.png)

![](./assets_image/E0403_05.png)

![](./assets_image/E0403_06.png)

![](./assets_image/E0403_07.png)

![](./assets_image/E0403_08.png)

![](./assets_image/E0403_09.png)

![](./assets_image/E0403_10.png)

![](./assets_image/E0403_11.png)

#### :bookmark: 实验内容03相关知识

暂无

## Futhermore

1. `Fitnesse`官方网址：<http://docs.fitnesse.org/FrontPage>
1. `Fitnesse release 20161106`：<http://docs.fitnesse.org/.FrontPage.FitNesseDevelopment.FitNesseRelease20161106>
