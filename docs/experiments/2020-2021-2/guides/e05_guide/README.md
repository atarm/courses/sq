# QA-E05： FitNesse的解析与二次开发

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [实验基本信息](#实验基本信息)
2. [实验注意事项](#实验注意事项)
3. [实验前提条件与储备知识](#实验前提条件与储备知识)
4. [实验指引](#实验指引)
    1. [实验内容01](#实验内容01)
        1. [:microscope: 实验内容01具体任务](#microscope-实验内容01具体任务)
        2. [:ticket: 实验内容01参考截图](#ticket-实验内容01参考截图)
        3. [:bookmark: 实验内容01相关知识](#bookmark-实验内容01相关知识)
    2. [实验内容02](#实验内容02)
        1. [:microscope: 实验内容02具体任务](#microscope-实验内容02具体任务)
        2. [:ticket: 实验内容02参考截图](#ticket-实验内容02参考截图)
        3. [:bookmark: 实验内容02相关知识](#bookmark-实验内容02相关知识)

<!-- /code_chunk_output -->

## 实验基本信息

1. 实验性质：设计性
1. 实验学时：4
1. 实验目的：
    1. 理解开源软件的解析
    1. 理解开源软件的二次开发
1. 实验内容与要求：
    1. 实验内容01： 下载、导入并构建`FitNesse`运行
    1. 实验内容02： 对`FitNesse`进行简单的二次开发进重新构建`FitNesse`运行
1. 实验条件：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 软件环境：`Eclipse`或`Intellij IDEA`（`JDK 1.8`或以上），推荐使用`Intellij IDEA`

## 实验注意事项

1. 实验指引中使用 **尖括号`<>`表示键盘按键** ，如：`<CTRL>`代表control键，`<j>`代表j键
1. 实验中请将`EN_and_FN_in_PY`改成您的 **学号末两位** 和 **姓名全名的拼音**，如“68zhangsan”
1. 实验内容与实验指引中各子内容包含三部分：
    1. **:microscope: 实验内容具体任务** ==> 实验的具体任务及步骤
    1. **:ticket: 实验内容参考截图** ==> 实验的参考结果截图
    1. **:bookmark: 实验内容相关知识** ==> 实验涉及的相关知识

## 实验前提条件与储备知识

## 实验指引

### 实验内容01

#### :microscope: 实验内容01具体任务

1. 下载`FitNesse`源码（本实验使用`tag 20161106`的`source code`）： <https://github.com/unclebob/fitnesse/releases/tag/20161106>
1. 在`IDEA`或`Eclipse`中导入下载的`FitNesse`源码
1. 运行`Gradle`的`standaloneJar`任务
1. 在`${project_root}/build/libs`下运行`java -jar fitnesse-${date}-standalone.jar -p 1234`，其中`${project_root}`表示`FitNesse`源码所在目录，`${date}`代表当前打包的时间（`FitNesse`源码构建打包成`jar`文件时会将当前打包时间作为`jar`文件名的一部分）
1. 在浏览器中打开`http://localhost:1234/`

#### :ticket: 实验内容01参考截图

![](./assets_image/E0501_01.png)

![](./assets_image/E0501_02.png)

![](./assets_image/E0501_03.png)

![](./assets_image/E0501_04.png)

![](./assets_image/E0501_05.png)

![](./assets_image/E0501_06.png)

#### :bookmark: 实验内容01相关知识

### 实验内容02

#### :microscope: 实验内容02具体任务

1. 在`IDEA`或`Eclipse`中打开`${project_root}/src/fitnesse/testsystems/ClientBuilder.java`
1. 为`DEFAULT_COMMAND_PATTERN`数组增加一个元素`"-Dfile.encoding=utf-8"`
1. 重新运行`Gradle`的`standaloneJar`任务

#### :ticket: 实验内容02参考截图

![](./assets_image/E0502_01.png)

![](./assets_image/E0502_02.png)

#### :bookmark: 实验内容02相关知识

## 延伸阅读Futhermore

1. [黑马程序员：Gradle精品公开课](http://yun.itheima.com/course/438.html)
