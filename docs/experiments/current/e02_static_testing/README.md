# 静态测试Static Testing

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [基本信息Basic Information](#基本信息basic-information)
2. [说明及注意事项Cautions](#说明及注意事项cautions)
3. [知识储备Preparation](#知识储备preparation)
4. [思考题Thinking](#思考题thinking)
5. [指引Guides](#指引guides)
    1. [内容01：创建`maven project`并编写被测模块](#内容01创建maven-project并编写被测模块)
        1. [创建`parent module`](#创建parent-module)
        2. [创建`child module`](#创建child-module)
        3. [编写被测模块](#编写被测模块)
    2. [内容02：`IDE`中安装并使用`static testing`](#内容02ide中安装并使用static-testing)
        1. [`CheckStyle-IDEA`静态分析](#checkstyle-idea静态分析)
        2. [`PMDPlugin`静态分析](#pmdplugin静态分析)
        3. [`Alibaba Java Coding Guidelines`静态分析](#alibaba-java-coding-guidelines静态分析)
        4. [`SpotBugs`静态分析](#spotbugs静态分析)
    3. [内容03：`maven`中配置并使用`static testing`](#内容03maven中配置并使用static-testing)
        1. [步骤01：配置静态分析插件的版本号](#步骤01配置静态分析插件的版本号)
        2. [步骤02：配置静态分析插件](#步骤02配置静态分析插件)
            1. [声明静态分析插件](#声明静态分析插件)
            2. [使用静态分析插件](#使用静态分析插件)
6. [延伸阅读Futhermore](#延伸阅读futhermore)

<!-- /code_chunk_output -->

## 基本信息Basic Information

1. 性质：设计性
1. 学时：6
1. 目的：
    1. 掌握`static testing`的概念
    1. 掌握`IDE`中使用`static testing`工具的方法
    1. 掌握`maven`中使用`static testing`工具的方法
1. 内容与要求：
    1. 内容01：创建`maven project`并编写被测模块
    1. 内容02：`IDE`中安装并使用`static testing`
    1. 内容03：`maven`中配置并使用`static testing`
1. 条件：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 软件环境：`IDEA`

## 说明及注意事项Cautions

1. 指引中使用 **尖括号`<>`表示键盘按键** ，如：`<CTRL>`代表`control`键，`<j>`代表`j`键, `<CTRL - j>`代表同时按下`control`和`j`键
1. 指引中的 **`"${FN_and_EN}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，如 **"zhangsan68"**

## 知识储备Preparation

1. `Maven`：参考《Maven实践》第01-09章、第10章

## 思考题Thinking

🤔 请思考：

1. `CheckStyle`、`PMD`、`SpotBugs`、`Alibaba Java Coding Guidelines`/`P3C`之间有什么相同点和不同点？
1. 在`IDE`中使用静态分析插件与在`Maven`中使用静态分析插件有哪些区别？各有什么优缺点？
1. "内容03"的静态测试缺少哪些重要节点？可能的解决方案有哪些？

## 指引Guides

### 内容01：创建`maven project`并编写被测模块

#### 创建`parent module`

🚉 操作流程：

1. 从`IDEA`中新建一个`maven project`
    1. `groupId`设置为`crse.sq`
    1. `artifactId`设置成`"testing-parent-${FN_and_EN}"`（请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，如 **"zhangsan68"**）❗ 以下将该`module`称为`testing-parent` ❗
1. 在`testing-parent`的`pom.xml`中的`packaging`属性设置为`pom`

#### 创建`child module`

🚉 操作流程：

1. 从`IDEA`中新建一个`maven module`
    1. `groupId`设置为`crse.sq`
    1. `artifactId`设置成`"testing-static-${FN_and_EN}"`（请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，如 **"zhangsan68"**）❗ 以下将该`module`称为`testing-static` ❗
1. 在`testing-static`的`pom.xml`中将`testing-parent`设置为父模块
1. 在`testing-parent`的`pom.xml`中将`testing-static`设置为聚合模块

`testing-parent`的`pom.xml`设置参考如下（⚠️ 请将以下配置中的`artifactId`配置成您的`artifactId` ⚠️）

```xml {.line-numbers}
<project>
    <groupId>org.arm.crse.sq</groupId>
    <artifactId>testing-parent</artifactId>
    <packaging>pom</packaging>
    <version>1.0-SNAPSHOT</version>
    <modules>
        <module>static</module>    <!-- ❗ it is filepath, not artifactId ❗ -->
    </modules>
</project>
```

`testing-static`的`pom.xml`设置参考如下（⚠️ 请将以下配置中的`artifactId`配置成您的`artifactId` ⚠️）

```xml {.line-numbers}
<project>
   <parent>
        <artifactId>testing-parent</artifactId>
        <groupId>org.arm.crse.sq</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
</project>
```

#### 编写被测模块

在`testing-static`中新增被测类（❗ 可为任意类，以下仅为参考 ❗）

```java {.line-numbers}
public class Calculator {
    public int add(int a, int b) {
        return a + b;
    }
}
```

### 内容02：`IDE`中安装并使用`static testing`

🚉 参考操作流程：

1. 安装插件：`Settings>>>Plugins`打开插件安装对话框，安装`CheckStyle-IDEA`、`PMDPlugin`、`Alibaba Java Coding Guidelines`、`SpotBugs`插件
1. 运行`CheckStyle-IDEA`静态分析并查看分析报告
1. 运行`PMDPlugin`静态分析并查看分析报告
1. 运行`Alibaba Java Coding Guidelines`静态分析并查看分析报告
1. 运行`SpotBugs`静态分析并查看分析报告（⚠️ `SpotBugs`对新版本的`IDEA`可能出现兼容性故障报告 ⚠️）

#### `CheckStyle-IDEA`静态分析

![CheckStyle-IDEA](./.assets/image/checkstyle-idea.png)

#### `PMDPlugin`静态分析

右键点击待分析的`project`或`module`或`package`或`class`。

![PMDPlugin](./.assets/image/pmdplugin.png)

#### `Alibaba Java Coding Guidelines`静态分析

右键点击待分析的`project`或`module`或`package`或`class`。

![alibaba](./.assets/image/alibaba.png)

#### `SpotBugs`静态分析

![SpotBugs](./.assets/image/spotbugs.png)

### 内容03：`maven`中配置并使用`static testing`

🚉 参考操作流程：

1. 配置静态分析插件的版本号：在`testing-parent`的`pom.xml`中配置版本号
1. 配置静态分析插件：在`testing-parent`的`pom.xml`中配置`maven-jxr-plugin`、`maven-checkstyle-plugin`、`maven-pmd-plugin`、`spotbugs-maven-plugin`
1. 生成静态分析报告：在`testing-parent`或`testing-static`中运行`mvn site`
1. 查看静态分析报告；打开`testing-static`下的`target/site/index.html`查看静态分析报告
    1. `CheckStyle`静态分析报告: `Project Documentation>>>Project Reports>>>CheckStyle`
    1. `PMD`&&`P3C`静态分析报告:
        1. `Project Documentation>>>Project Reports>>>CPD`
        1. `Project Documentation>>>Project Reports>>>PMD`
    1. `SpotBugs`静态分析报告: `Project Documentation>>>Project Reports>>>SpotBugs`

#### 步骤01：配置静态分析插件的版本号

在`testing-parent`的`pom.xml`的`<properties>`进行以下配置。

```xml {.line-numbers}
<project>
    <properties>
        <plugin.jxr.version>3.2.0</plugin.jxr.version>
        <plugin.checkstyle.version>3.1.2</plugin.checkstyle.version>
        <plugin.pmd.version>3.16.0</plugin.pmd.version>
        <plugin.pmd.p3c.version>2.1.1</plugin.pmd.p3c.version>
        <plugin.spotbugs.version>4.6.0.0</plugin.spotbugs.version>
    </properties>
</project>
```

#### 步骤02：配置静态分析插件

##### 声明静态分析插件

在`testing-parent`的`pom.xml`的`<pluginManagement>`下进行以下配置

```xml {.line-numbers}
<project>
    <build>
        <pluginManagement>
        <plugins>
    <!--#region maven-jxr-plugin-->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jxr-plugin</artifactId>
                <version>${plugin.jxr.version}</version>
            </plugin>
    <!--#endregion maven-jxr-plugin-->

    <!--#region maven-checkstyle-plugin-->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
                <version>${plugin.checkstyle.version}</version>
            </plugin>
    <!--#endregion maven-checkstyle-plugin-->

    <!--#region maven-pmd-plugin-->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-pmd-plugin</artifactId>
                <version>${plugin.pmd.version}</version>
                <configuration>
                    <rulesets>
                        <!-- PMD ruleset -->
                        <ruleset>/category/java/bestpractices.xml</ruleset>
                        <ruleset>/category/java/codestyle.xml</ruleset>
                        <ruleset>/category/java/design.xml</ruleset>
                        <ruleset>/category/java/documentation.xml</ruleset>
                        <ruleset>/category/java/errorprone.xml</ruleset>
                        <ruleset>/category/java/multithreading.xml</ruleset>
                        <ruleset>/category/java/performance.xml</ruleset>
                        <!-- P3C ruleset -->
                        <ruleset>rulesets/java/ali-comment.xml</ruleset>
                        <ruleset>rulesets/java/ali-concurrent.xml</ruleset>
                        <ruleset>rulesets/java/ali-constant.xml</ruleset>
                        <ruleset>rulesets/java/ali-exception.xml</ruleset>
                        <ruleset>rulesets/java/ali-flowcontrol.xml</ruleset>
                        <ruleset>rulesets/java/ali-naming.xml</ruleset>
                        <ruleset>rulesets/java/ali-oop.xml</ruleset>
                        <ruleset>rulesets/java/ali-orm.xml</ruleset>
                        <ruleset>rulesets/java/ali-other.xml</ruleset>
                        <ruleset>rulesets/java/ali-set.xml</ruleset>
                    </rulesets>
                    <printFailingErrors>true</printFailingErrors>
                </configuration>
    <!--#region p3c-pmd-->
                <dependencies>
                    <!-- https://mvnrepository.com/artifact/com.alibaba.p3c/p3c-pmd -->
                    <dependency>
                        <groupId>com.alibaba.p3c</groupId>
                        <artifactId>p3c-pmd</artifactId>
                        <version>${plugin.pmd.p3c.version}</version>
                    </dependency>
                </dependencies>
    <!--#endregion p3c-pmd-->
            </plugin>
    <!--#endregion maven-pmd-plugin-->

    <!--#region spotbugs-maven-plugin-->
            <plugin>
                <groupId>com.github.spotbugs</groupId>
                <artifactId>spotbugs-maven-plugin</artifactId>
                <version>${plugin.spotbugs.version}</version>
            </plugin>
    <!--#endregion spotbugs-maven-plugin-->
        </plugins>
        </pluginManagement>
    </build>
</project>
```

##### 使用静态分析插件

在`testing-parent`的`pom.xml`中的`<reporting>`进行以下配置

```xml {.line-numbers}
<project>
   <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-site-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jxr-plugin</artifactId>
            </plugin>
        </plugins>
    </reporting>
</project>
```

在`testing-static`的`pom.xml`中的`<reporting>`进行以下配置

```xml {.line-numbers}
<project>
    <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-pmd-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>com.github.spotbugs</groupId>
                <artifactId>spotbugs-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </reporting>
</project>
```

## 延伸阅读Futhermore

1. [Using the SpotBugs Maven Plugin.](https://spotbugs.readthedocs.io/en/latest/maven.html)
