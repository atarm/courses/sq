# Experiments

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [Contents](#contents)
2. [Futhermore](#futhermore)

<!-- /code_chunk_output -->

## Contents

<!-- ```bash {.line-numbers cmd=true output=text hide=false run_on_save=true}
tree -I *_code_chunk -I *.html -I *.pdf -d -L 1 .
``` -->

```bash {.line-numbers}
.
├── e01_unit_testing                      单元测试技术
├── e02_static_testing                    静态测试技术
└── e03_continuous_integration            集成测试技术
```

## Futhermore

1. [廖雪峰. Java教程-Maven基础.](https://www.liaoxuefeng.com/wiki/1252599548343744/1255945359327200)
1. [廖雪峰. Java教珵-单元测试.](https://www.liaoxuefeng.com/wiki/1252599548343744/1255945269146912)
