# 单元测试Unit Testing

## 基本信息Basic Information

1. 性质：设计性
1. 学时：6
1. 目的：
    1. 掌握`unit testing`的概念、工具和方法
    1. 掌握`black box`和`white box`测试用例设计方法
    1. 掌握`maven`、`junit5`、`jacoco`执行测试的方法
1. 内容与要求：
    1. 内容01：使用`black box`，为"Triangle"设计并运行测试用例
    1. 内容02：使用`white box`，为"SampleLogic"设计并运行测试用例
1. 条件：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 软件环境：`IDEA`或其他`Java`开发环境

## 说明及注意事项Cautions

1. 指引中使用 **尖括号`<>`表示键盘按键** ，如：`<CTRL>`代表`control`键，`<j>`代表`j`键, `<CTRL - j>`代表同时按下`control`和`j`键
1. 指引中的 **`"${FN_and_EN}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，如 **"zhangsan68"**

## 问题描述Problems

### 内容01：使用黑盒测试方法，为"Triangle"设计并运行测试用例

对`a`，`b`，`c`3个整数是否构成三角形、构成何种三角形进行判断，具体规则如下：

1. 3个整数必须大于`0`且小于`101`，否则提示错误
1. 能构成三角形的充要条件是：任意两个整数之和大于第三个整数
1. 构成等腰三角形的充要条件是：能构成三角形的基础上，恰好有两个整数相等
1. 构成等边三角形的充要条件是：能构成三角形的基础上，三个整数相等

请使用`ECP`和`BVA`为该程序设计并运行测试用例。

### 内容02：使用白盒测试方法，为"SampleLogic"设计并运行测试用例

`SampleLogic`源码如下：

```java {.line-numbers}
public int process(int x, int y) {
    //s1
    int ret = 0;
    if (x == 0 && y > 2) {
        //s2
        ret += 1;
    } 
    if (x < 1 || y == 1) {
        //s3
        ret += 2;
    }
    //s4
    ret += x + y;
    return ret;
}
```

请使用逻辑覆盖法，为该程序设计`语句覆盖`、`判定覆盖`和`条件覆盖`测试用例，并运行测试用例

## 指引Guides

### Maven配置

1. 新建一个`maven project`
    1. `groupId`设置成`crse.sq`
    1. `artifactId`设置成`"${FN_and_EN}`（请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，如 **"zhangsan68"**）
1. 引入依赖
    1. `junit5`
1. 配置`jacoco-maven-plugin`

### 编写被测类和测试用例

1. 编写被测类`Triangle`和`SampleLogic`：被测类位于`crse.sq.${FN_and_EN}`下（请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，如 **"zhangsan68"**）
1. 设计并编写`Triangle`的测试用例数据`triangle_ecp_and_bva.csv`
1. 设计并编写`Triangle`的测试用例程序`TriangleTest`
    1. 通过`@ParameterizedTest`标注为参数化测试
    1. 通过`@CsvFileSource`导入参数化测试数据
1. 设计并编写`SampleLogic`的测试用例数据
    1. 语句覆盖：`samplelogic_statement_coverage.csv`
    1. 判定覆盖：`samplelogic_branch_coverage.csv`
    1. 条件覆盖：`samplelogic_condition_coverage.csv`
1. 设计并编写`SampleLogic`的测试用例程序`SampleLogicTest`
    1. 通过`@ParameterizedTest`标注为参数化测试
    1. 通过`@CsvFileSource`导入参数化测试数据

### 运行测试用例并查看测试报告

1. 运行`mvn test`
1. 查看测试报告：
    1. `target/coverage/jacoco/index.html`
    1. `target/surefire-reports/*.txt`

### 参考代码

参考代码请见`${repo_base}/codes/testing_sample/`

请将下列结构中的包结构替换成您的结构❗

```bash {.line-numbers}
.
├── pom.xml
├── src
│   ├── main
│   │   └── java
│   │       └── org
│   │           └── arm
│   │               └── crse
│   │                   └── sq
│   │                       ├── SampleLogic.java
│   │                       └── Triangle.java
│   └── test
│       ├── java
│       │   └── org
│       │       └── arm
│       │           └── crse
│       │               └── sq
│       │                   ├── blackbox
│       │                   │   └── TriangleTest.java
│       │                   └── whitebox
│       │                       └── SampleLogicTest.java
│       └── resources
│           └── usecases
│               ├── blackbox
│               │   └── triangle_ecp_and_bva.csv
│               └── whitebox
│                   ├── samplelogic_branch_coverage.csv
│                   ├── samplelogic_condition_coverage.csv
│                   └── samplelogic_statement_coverage.csv
└── testing-sample.iml
```

❓ 思考以下问题：

1. `jacoco`的`branch coverage`与理论定义的`branch coverage`有什么不一样？

## Futhermore

1. [jetbrains.Run with coverage.](https://www.jetbrains.com/help/idea/running-test-with-coverage.html)
