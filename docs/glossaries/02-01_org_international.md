# 国际机构/国际标准相关术语

## 国际标准化组织（International Organization for Standardization, `ISO`）

## 国际电工委员会（International Electrotechnical Commission, `IEC`）

## 国际电信联盟（International Telecommunication Union, `ITU`）

## 万维网联盟（World Wide Web Consortium, `W3C`）

## Linux基金会（The Linux Foundation, `LF`）

## 欧洲计算机制造联合会（European Computer Manufactures Association, `ECMA`）

## 欧洲电信标准协会（European Telecommunications Standards Institute, `ETSI`）
