# 国家机构/国家标准相关术语

## 中华人民共和国国家标准（GuoBiao, `GB`）

## 美国国家标准协会（American National Standards Institute, `ANSI`）

## 美国联邦信息处理标准（Federal Information Processing Standards, `FIPS`）

## 英国国家标准协会（British Standard Institute, `BSI`）

## 德国国家标准协会（Deutsches Institut für Normung, `DIN`）

## 日本工业标准（Japanese Industrial Standard, `JIS`）
