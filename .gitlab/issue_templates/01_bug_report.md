<!--
---
name: 🐛 bug report
about: report a bug
---
🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅

oh hi there! 😄

to expedite issue processing please search open and closed issues before submitting a new one.

existing issues often contain information about workarounds, resolution, or progress updates.

🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅
-->

<!--
this is borrow from:

https://raw.githubusercontent.com/angular/angular/master/.github/ISSUE_TEMPLATE/1-bug-report.md

https://raw.githubusercontent.com/stevemao/github-issue-templates/master/bugs-only/ISSUE_TEMPLATE.md
-->

# 🐛 Bug Report

## 📨 Short Description

<!-- 🖊️ a clear and concise description of the problem... -->

## 🔥 About The Problem

### :camera_flash: Snapshoot

<!-- 🖊️ often a screenshot can help to capture the issue better than a long description. -->
<!-- 🖊️ upload a screenshot:-->

### 💻 Your Context (Environment)

<!-- 🖊️ How has this issue affected you? What are you trying to accomplish? -->
<!-- 🖊️ Providing context helps us come up with a solution that is most useful in the real world -->

<!-- 🖊️ Provide a general summary of the issue in the Title above -->

### 😋 Expected Behavior

<!-- 🖊️ tell us what should happen -->

### 🙃 Actual Behavior

<!-- 🖊️ tell us what happens instead of the expected behavior -->

### 😠 Is This a Regression?

<!-- 🖊️ did this behavior use to work in the previous version? -->

<!-- 🖊️ yes, the previous version in which this bug was not present was: .... -->

### 🚶 Steps to Reproduce

<!--- provide a link to a live example, or an unambiguous set of steps to -->
<!--- reproduce this bug. Include code to reproduce, if relevant -->

1. <!-- 🖊️ step description here-->
1. <!-- 🖊️ step description here-->
1. <!-- 🖊️ step description here-->
1. <!-- 🖊️ step description here-->

## 😮 About Your Trying

### 💪 What You Had Try

### 🔧 Possible Solution
<!-- not obligatory, but suggest a fix/reason for the bug, -->

## 🔗 Anything Else Relevant?
<!-- please provide additional info if necessary. -->
