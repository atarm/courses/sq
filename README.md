# Software Quality – Assurance and Testing软件质量：保证与测试

又名：《软件质量保证与测试》或《软件测试技术》

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [关于本仓库](#关于本仓库)
    1. [许可协议](#许可协议)
    2. [目录组织](#目录组织)
    3. [`emoji`](#emoji)
    4. [帮助完善](#帮助完善)
2. [Futhermore](#futhermore)
    1. [Books and Monographs](#books-and-monographs)
    2. [Courses and Tutorials](#courses-and-tutorials)
    3. [Papers and Articles](#papers-and-articles)

<!-- /code_chunk_output -->

## 关于本仓库

### 许可协议

![CC-BY-SA-4.0](./.assets/LICENSES/cc-by-sa-4.0/image/CC_BY-SA_88x31.png)

采用`CC-BY-SA-4.0`协议，署名：aRoming。

<!--
### 编写环境和使用方法

请详见<https://gitlab.com/arm_commons/commons/-/blob/master/ENVS/MD_RAW/README.md>
-->

### 目录组织

```sh
.
├── .editorconfig       ==> EditorConfig
├── .git                ==> git repository
├── .gitignore          ==> gitignore
├── README.md           ==> 本文件
├── codes               ==> 代码
└── docs                ==> 文档
    ├── abouts          ==> 相关介绍，如：大纲等
    ├── addons          ==> 附加内容，如：专题论述等
    ├── exercises       ==> 习题及其解析
    ├── experiments     ==> 实践指引及FAQ
    ├── glossaries      ==> 术语表
    ├── lectures        ==> 讲义
    └── slides          ==> 幻灯片/课件/PPT（在`VSCode`上使用`MARP`编辑）
```

### `emoji`

1. 资源形式：
    1. 📖 纸质出版物
    1. 💻 电子出版物
    1. 🌐 互联网资源
1. 评价等级：
    1. 👑 顶级好评（权威）
    1. 👍 一级好评
    1. 🌟 二级好评，或重要
    1. ⭐ 一般好评，或次重要
1. 费用情况：
    1. 🆓 免费
    1. 💰 收费
1. 其他：
    1. 🏛️ 官方
    1. ❗ 注意
    1. ❓ 有歧义或有瑕疵

### 帮助完善

期待您一起完善，您可以通过以下方式帮助完善：

1. **`merge request`**：通过`GitLab`的`merge request`到`master`分支
1. **`issue`**： 通过`GitLab`的`issue`发起一个新的`issue`（标签设置成`optimize`）

仓库地址：

1. 主地址： <https://gitlab.com/atarm/courses/sq>
1. 镜像地址： <https://jihulab.com/atarm/courses/sq>

## Futhermore

### Books and Monographs

1. 📖 👑（美）Glenford J.Myers, Tom Badgett, Corey Sandler 著. 张晓明, 黄琳 译.[《软件测试的艺术（原书第3版）》[M]](http://www.hzcourse.com/web/refbook/detail/4557/219).北京:机械工业出版社.2012-04.ISBN:9787111376606.
1. 📖 👑 （美）James Whittaker，（美）Jason Arbon，（美）Jeff Carollo 著，黄利，李中杰，薛明 译. Google软件测试之道（_"How Google Tests Software"_）. 北京:人民邮电出版社. 2013-10. ISBN:978-7-115-33024-6.
1. 📖 秦航、杨强 著. [软件质量保证与测试（第2版）[M]](http://www.tup.tsinghua.edu.cn/booksCenter/book_07226401.html).北京:清华大学出版社.2017-07.ISBN:9787302467632.
1. 📖 朱少民,马海霞,王新颖,刘冉,蒋琦,吴振宇,蔡秋亮等编著.[软件测试实验教程[M]](http://www.tup.tsinghua.edu.cn/booksCenter/book_07832901.html).北京:清华大学出版社.2019-06.ISBN:9787302523734.
1. 📖 王智钢,杨乙霖（主编）.[软件质量保证与测试（慕课版）[M]](https://www.ptpress.com.cn/shopping/buy?bookId=ccdfa964-ff89-4c65-9c80-5d957b64d189).北京:中国工信出版集团,北京:人民邮电出版社.2020-10.ISBN:9787115542212
1. 📖 黑马程序员.[《软件测试》[M]](https://www.ryjiaoyu.com/book/details/39563). 北京:人民邮电出版社.2019-10.ISBN:9787115515230.
1. 📖 Jeff Langr, Andy Hunt, Dave Thomas.[_"Pragmatic Unit Testing in Java 8 with JUnit_"[M]](https://www.oreilly.com/library/view/pragmatic-unit-testing/9781680500769/).Oreilly.2015-03.ISBN:9781941222591.

### Courses and Tutorials

1. 💻 华中科技大学 - 软件测试与质量： <https://www.icourse163.org/course/HUST-1001907003>
1. 💻 Universiti Tenaga Nasional(Malaysia) - CSEB453 Software Quality: <http://metalab.uniten.edu.my/~hazleen/CSEB453/>

### Papers and Articles

1. 💻 GeeksforGeeks Software Testing: <https://www.geeksforgeeks.org/software-testing-basics/>
1. 💻 Software Testing Fundamentals (STF) !: <https://softwaretestingfundamentals.com/>
